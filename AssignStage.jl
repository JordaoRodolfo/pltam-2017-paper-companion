module AssignStage

export ARC, ARC2, ARC_CiR, ARC_InO, ARC_Gen
export buildModel
export MILP
export InOOptim, GenOptim, CiROptim, OOSOptim, ReSCOptim

using ImplementationTypes
using JuMP
using Base.Cartesian
using StatsBase

function buildModel(buildDict)
	assignDict = copy(buildDict)
	base_specs = buildDict["Base"]
	N = buildDict["Nodes"]
	agents = buildDict["Agents"]
	num_agents = length(agents)
	num_operations = buildDict["Operations"]

	orientations = buildDict["Orientations"]#Dict{Int, Float64}()
	#for (k, v) in buildDict["Orientations"]
	#orientations[k] = v
	#end

	num_orientations = length(orientations)
	positions = buildDict["Positions"]
	interest = buildDict["Interest"]
	interest_nodes = buildDict["InterestNodes"]
	boundaries = buildDict["Boundaries"]
	obstacles = buildDict["Obstacles"]

	assignDict["Betas"] = Dict(a => agents[a]["beta"] for a in keys(agents))

	relation = Dict{Int,Int}()
	invrelation = Dict{Int,Int}()
	for (k, v) in enumerate(interest_nodes)
		relation[v] = k
		invrelation[k] = v
	end
	assignDict["Relation"] = relation

	NodeCosts = zeros(N, num_operations)
	for (n, c) in buildDict["NodeCosts"]
		NodeCosts[relation[n], :] = c
	end
	assignDict["NodeCosts"] = NodeCosts
	assignDict["Demand"] = NodeCosts .> 0.0

	supply = zeros(num_agents, num_operations)
	for (na, va) in agents
		for (no, vo) in va["operation"]
			supply[na, no] = 1
		end
	end
	assignDict["Supply"] = supply

	Costs = zeros(N, N, num_orientations, num_orientations, num_agents)
	#for i in 1:N
	#for j in 1:N
	#for b in 1:num_orientations
	#for e in 1:num_orientations
	#for a in 1:num_agents
	#c = buildDict["Costs"][invrelation[i], invrelation[j], b, e, a]
	#Costs[i,j,b,e,a] = c
	#end
	#end
	#end
	#end
	#end
	for (i, v1) in buildDict["Costs"]
		for (j, v2) in v1
			for (b, v3) in v2
				for (e, v4) in v3
					for (a, c) in enumerate(v4)
						Costs[relation[i], relation[j], b, e, a] = c
					end
				end
			end
		end
	end
	assignDict["Costs"] = Costs

	#Pathstrings = buildDict["Paths"]#Dict((1,2,1) => [1; 2])
	#Paths = Dict{Tuple{State, State, Int}, Array{State, 1}}()
	# sta = r"\((\d+),(\d+)\)"
	# ag = r",(\d+)\)$"
	#for (k, v) in Pathstrings
	# println(k)
	#ar = fill(State(1,1), length(v))
	#tup = eval(parse(k))
	#for (num, val) in v
	#sta = eval(parse(val))
	#ar[num] = sta
	#end
	#Paths[tup] = ar
	# agcap = match(ag, k)
	# println(tup)
	#end
	# println(Paths)
	# for (i, v1) in buildDict["Paths"]
	#   for (j, v2) in v1
	#     for (a, p) in v2
	#       Paths[(i,j,a)] = p
	#     end
	#   end
	# end
	startStates = Dict{Int, State}()
	for (a, s) in buildDict["BaseInbound"]
		startStates[a] = State(relation[s.node], s.orientation)
	end
	endStates = Dict{Int, State}()
	for (a, s) in buildDict["BaseOutbound"]
		endStates[a] = State(relation[s.node], s.orientation)
	end
	assignDict["StartStates"] = startStates
	assignDict["EndStates"] = endStates

	return assignDict
end

function hasintersection(t1::Tour, t2::Tour)
	if t1.a == t2.a
		for i in t1[2:end-1]
			if i in t2[2:end-1]
				return true
			end
		end
	end
	return false
end

function equalTourArrays(Ri::Array{Tour, 1}, Rj::Array{Tour, 1})
	if size(Ri, 1) != size(Rj, 1)
		return false
	end
	for ri in Ri
		equals = false
		for rj in Rj
			equals = equals || isequal(ri, rj)
		end
		if !equals
			return false
		end
	end
	return true
end

function tourInsideSet(q, Q)
	inside = false
	for qj in Q
		inside = inside || equalTourArrays(q, qj)
	end
	return inside
end

modn(x, n) = x > n ? x - n : x

function pushsorted!(v, x)
	i = 1
	n = size(v, 1)
	while i <= n && v[i] <= x
		i += 1
	end
	insert!(v, i, x)
end

function tourCost(tour::Tour, NodeCosts, Costs, supply)
	# must start and end in the base_n
	itb = first(tour)
	cost = sum(NodeCosts[itb.node, o]*supply[tour.a, o] for o in 1:size(supply, 2))
	for it in Iterators.drop(tour, 1)
		cost += Costs[itb.node, it.node, itb.orientation, it.orientation, tour.a]
  	cost += sum(NodeCosts[it.node, o]*supply[tour.a, o] for o in 1:size(supply, 2))
		#if opcost < sum(NodeCosts[it.node, :])
			#opcost = sum(NodeCosts[it.node, :])
		#end
		itb = it
	end
	return cost
end

function H(R::Array{Tour}, NodeCosts, Costs, supply)
	# h += tourCost(R, NodeCosts, DirectCosts, TurnCosts)
	return sum(map(r -> tourCost(r, NodeCosts, Costs, supply), R))
end

function removalCost(R::Tour, pos::Int, NodeCosts, Costs, supply)
	cost = Costs[R[pos-1].node, R[pos+1].node, R[pos-1].orientation, R[pos+1].orientation, R.a]
	cost -= Costs[R[pos-1].node, R[pos].node, R[pos-1].orientation, R[pos].orientation, R.a]
	cost -= Costs[R[pos].node, R[pos+1].node, R[pos].orientation, R[pos+1].orientation, R.a]
	cost -= sum(NodeCosts[R[pos].node, o]*supply[R.a, o] for o in 1:size(supply, 2))
	return cost
end

function bestRemovalCost(R::Tour, NodeCosts, Costs, supply)
	variation = 0.0
	mark =  2
	for i in 2:length(R)-1
		if removalCost(R, i, NodeCosts, Costs, supply) < variation
			variation = removalCost(R, i, NodeCosts, Costs, supply)
			mark = i
		end
	end
	return variation, mark
end

function removal(R::Array{Tour}, NodeCosts, Costs, tour::Int, pos::Int)
	Ri = deepcopy(R)
	deleteat!(Ri[tour], pos)
	return Ri
end

function insertionCost(R::Tour, newnode::Int, pos::Int, orientations::Dict{Int, Float64}, NodeCosts, Costs, supply)
	cost = Inf
	emin = 1
	for e in keys(orientations)
		c = Costs[R[pos-1].node, newnode, R[pos-1].orientation, e, R.a] + Costs[newnode, R[pos].node, e, R[pos].orientation, R.a]
		if -Inf < c < cost
			cost = c
			emin = e
		end
	end
	cost -= Costs[R[pos-1].node, R[pos].node, R[pos-1].orientation, R[pos].orientation, R.a]
	cost += sum(NodeCosts[newnode, o]*supply[R.a, o] for o in 1:size(supply, 2))
	return cost, emin
end

function mergeCost(tour1::Tour, tour2::Tour, Costs)
	var12 = Costs[tour1[end-1].node, tour2[2].node, tour1[end-1].orientation, tour2[2].orientation, tour1.a]
	var12 -= Costs[tour1[end-1].node, tour1[end].node, tour1[end-1].orientation, tour1[end].orientation, tour1.a]
	var12 -= Costs[tour2[1].node, tour2[2].node, tour2[1].orientation, tour2[2].orientation, tour1.a]
	var21 = Costs[tour2[end-1].node, tour1[2].node, tour2[end-1].orientation, tour1[2].orientation, tour1.a]
	var21 -= Costs[tour2[end-1].node, tour2[end].node, tour2[end-1].orientation, tour2[end].orientation, tour1.a]
	var21 -= Costs[tour1[1].node, tour1[2].node, tour1[1].orientation, tour1[2].orientation, tour1.a]
	if var12 <= var21
		return var12, 1
	else
		return var21, -1
	end
end

function switchCost(tour::Tour, pos1, pos2, orientations, Costs)
	cost = 0.0
	a = tour.a
	e1 = 1
	e2 = 1
	if pos2 - pos1 == 1
		for b in keys(orientations)
			for e in keys(orientations)
				varij = Costs[tour[pos1-1].node, tour[pos2].node, tour[pos1-1].orientation, b, a] +
				Costs[tour[pos2].node, tour[pos1].node, b, e, a] + Costs[tour[pos1].node, tour[pos2+1].node, e, tour[pos2+1].orientation, a]
				varij -= (Costs[tour[pos1-1].node, tour[pos1].node, tour[pos1-1].orientation, tour[pos1].orientation, a] +
				Costs[tour[pos1].node, tour[pos2].node, tour[pos1].orientation, tour[pos2].orientation, a] + Costs[tour[pos2].node, tour[pos2+1].node, tour[pos2].orientation, tour[pos2+1].orientation, a])
				if -Inf < varij < cost
					cost = varij
					e1 = b
					e2 = e
				end
			end
		end
	else
		varij = Inf
		for e in keys(orientations)
			c = Costs[tour[pos1-1].node, tour[pos2].node, tour[pos1-1].orientation, e, a] +  Costs[tour[pos2].node, tour[pos1+1].node, e, tour[pos1+1].orientation, a]
			c -= (Costs[tour[pos1-1].node, tour[pos1].node, tour[pos1-1].orientation, tour[pos1].orientation, a] + Costs[tour[pos1].node, tour[pos1+1].node, tour[pos1].orientation, tour[pos1+1].orientation, a])
			if -Inf < c < varij
				varij = c
				e1 = e
			end
		end
		varji = Inf
		for e in keys(orientations)
			c = Costs[tour[pos2-1].node, tour[pos1].node, tour[pos2-1].orientation, e, a] +  Costs[tour[pos1].node, tour[pos2+1].node, e, tour[pos2+1].orientation, a]
			c -= (Costs[tour[pos2-1].node, tour[pos2].node, tour[pos2-1].orientation, tour[pos2].orientation, a] + Costs[tour[pos2].node, tour[pos2+1].node, tour[pos2].orientation, tour[pos2+1].orientation, a])
			if -Inf < c < varji
				varji = c
				e2 = e
			end
		end
		cost = varij + varji
	end
	return cost, e1, e2
end

function reorientationCost(t::Tour, pos::Int, orientations::Dict{Int, Float64}, Costs)
	variation = 0.0
	ori = t[pos].orientation
	for e in keys(orientations)
		c = Costs[t[pos-1].node, t[pos].node, t[pos-1].orientation, e, t.a] + Costs[t[pos].node, t[pos+1].node, e, t[pos+1].orientation, t.a] -
		(Costs[t[pos-1].node, t[pos].node, t[pos-1].orientation, t[pos].orientation, t.a] + Costs[t[pos].node, t[pos+1].node, t[pos].orientation, t[pos+1].orientation, t.a])
		if -Inf < c < variation
			variation = c
			ori = e
		end
	end
	return variation, ori
end

function updateIncidence(tour::Tour, incidence::Dict{Int,Dict{Int,Int}})
	# println(tour)
	for (place, val) in enumerate(tour[2:end-1])
		# +1 is beacuase enumarate starts from 1 and not 2
		if !haskey(incidence[place+1], val.node)
			incidence[place+1][val.node] = 0
		end
		incidence[place+1][val.node] += 1
	end
	#println(incidence)
	return incidence
end

function leastIncident(basetour::Tour, orientations, incidence::Dict{Int,Dict{Int,Int}})
	states = fill(State(0, 0), length(basetour))
	states[1] = basetour[1]
	states[end] = basetour[end]
	a = basetour.a
	oris = collect(keys(orientations))
	#nodes = fill(0, len-2)
	# oris = fill(1, len)
	# oris[1] = startState.orientation
	# oris[end] = endState.orientation
	for (place, v) in incidence
		node, count = first(v)
		for (n, c) in v
			if !(n in map(s->s.node, states)) && c < count
				count = c
				node = n
			end
		end
		states[place].node = node
		states[place].orientation = rand(oris)
		#nodes[place-1] = state.node
	end
	tour = Tour(states, a)
	return tour
end

function HillClimb(tour::Tour, orientations, NodeCosts, Costs, supply; maxRepeats=100)
	n = length(tour)
	if n <= 3
		return tour, 0
	end
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	bestcost = tourCostVal(tour)
	besttour = deepcopy(tour)
	iters = 1
	variation = -1.0
	while variation < 0
		variation = 0.0
		choicei, choicej, b, f, op = 0, 0, 0, 0, 0
		#println(tour, " ", tourCostVal(tour))
		for i in 2:length(tour)-1
			for j in i+1:length(tour)-1
				c, orib, orif = switchCost(tour, i, j, orientations, Costs)
				if c < variation
					variation = c
					#println("op 1 ", variation)
					choicei, choicej, b, f, op = i, j, orib, orif, 1
				end
			end
			c, ori = reorientationCost(tour, i, orientations, Costs)
			if c < variation
				variation = c
				#println("op 2 ", variation)
				choicei, b, op = i, ori, 2
			end
		end
		if op == 1
			tour[choicei], tour[choicej] = State(tour[choicej].node, b), State(tour[choicei].node, f)
		elseif op == 2
			tour[choicei].orientation = b
		else
			return tour, tourCostVal(tour)
		end
		iters += 1
	end
	return besttour, iters
end

function CiROptim(tour::Tour, orientations, NodeCosts, Costs, supply, maxRepeats=100)
	n = length(tour)
	if n <= 3
		return tour, 0
	end
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	bestcost = tourCostVal(tour)
	besttour = deepcopy(tour)
	iters = 1
	repeat = 0
	incidence = Dict(pos => Dict(s.node => 0 for s in tour[2:n-1]) for pos in 2:n-1)
	while repeat < maxRepeats
		incidence = updateIncidence(tour, incidence)
		choicei, choicej, b, f, op = 0, 0, 0, 0, 0
		variation = 0.0
		for i in 2:length(tour)-1
			for j in i+1:length(tour)-1
				c, orib, orif = switchCost(tour, i, j, orientations, Costs)
				if c < variation
					variation = c
					choicei, choicej, b, f, op = i, j, orib, orif, 1
				end
			end
			c, ori = reorientationCost(tour, i, orientations, Costs)
			if c < variation
				variation = c
				choicei, b, op = i, ori, 2
			end
		end
		if op == 1
			tour[choicei], tour[choicej] = State(tour[choicej].node, b), State(tour[choicei].node, f)
		elseif op == 2
			tour[choicei].orientation = b
		else
			if tourCostVal(tour) < tourCostVal(besttour)
				repeat = 0
				bestcost = tourCostVal(tour)
				besttour = deepcopy(tour)
			elseif isequal(tour, besttour)
				return besttour, iters
			end
			insert!(tour.states, 2, tour[end-1])
			deleteat!(tour.states, length(tour)-1)
			repeat += 1
		end
		iters += 1
	end
	return besttour, iters
end

function InOOptim(tour::Tour, orientations, NodeCosts::Array{Float64, 2}, Costs::Array{Float64, 5}, supply, maxRepeats=100)
	n = length(tour)
	if n <= 3
		return tour, 0
	end
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	bestcost = tourCostVal(tour)
	besttour = deepcopy(tour)
	iters = 1
	repeat = 0
	incidence = Dict(pos => Dict(s.node => 0 for s in tour[2:n-1]) for pos in 2:n-1)
	while repeat < maxRepeats
		incidence = updateIncidence(tour, incidence)
		choicei, choicej, b, f, op = 0, 0, 0, 0, 0
		variation = 0.0
		for i in 2:length(tour)-1
			for j in i+1:length(tour)-1
				c, orib, orif = switchCost(tour, i, j, orientations, Costs)
				if c < variation
					variation = c
					choicei, choicej, b, f, op = i, j, orib, orif, 1
				end
			end
			c, ori = reorientationCost(tour, i, orientations, Costs)
			if c < variation
				variation = c
				choicei, b, op = i, ori, 2
			end
		end
		if op == 1
			tour[choicei], tour[choicej] = State(tour[choicej].node, b), State(tour[choicei].node, f)
		elseif op == 2
			tour[choicei].orientation = b
		else
			if tourCostVal(tour) < tourCostVal(besttour)
				repeat = 0
				bestcost = tourCostVal(tour)
				besttour = deepcopy(tour)
			elseif isequal(tour, besttour)
				return besttour, iters
			end
			tour_least = leastIncident(tour, orientations, incidence)
			if isequal(tour, tour_least)
				return besttour, iters
			else
				tour = tour_least
			end
			repeat += 1
		end
		iters += 1
	end
	return besttour, iters
end

function AntOptim(tour::Tour, orientations, NodeCosts, Costs, supply; maxRepeats=10)
	n = length(tour)
	if n <= 3
		return tour, 0 
	end
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	bestcost = tourCostVal(tour)
	besttour = deepcopy(tour)
	iters = 1
	repeat = 0
end

function GenOptim(tour::Tour, orientations, NodeCosts, Costs, supply; maxPercent=0.95, populationsize=20, mut=0.4, maxGens=1000)
	n = length(tour)
	if n <= 3
		return tour, 0
	end
	#populationsize = populationfactor*n
	oris = collect(keys(orientations))
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	population = [tour]
	for i in 1:populationsize-1
  	push!(population, Tour([tour[1]; shuffle(tour[2:end-1]); tour[end]], tour.a))
	end
	bestcost = tourCostVal(tour)
	besttour = deepcopy(tour)
	iters = 1
	percent = 0.0
	#w = [tourCostVal(t) for t in population]
	while percent < maxPercent && iters < maxGens
		#fitnessVals = map(t -> tourCostVal(t), population)
  	#fitnessTotal = sum(fitnessVals)
		# build new population
		nextpopulation = Tour[]
		#for i in 1:populationsize
			#w[i] = tourCostVal(population[i])
		#end
		wv = weights([tourCostVal(t) for t in population])
		for i in 1:populationsize
  		p = sample(population, wv, 2, replace=false)
			p1, p2 = p[1], p[2]
			newtour = Tour([p1[1]], p1.a)
			possibles = Iterators.flatten((p1[2:n-1], p2[2:n-1]))
			for j in 1:n-2
				possibles = Iterators.filter(s -> !(s in newtour), possibles)
				state = newtour[j]
				finite = Iterators.filter(s -> Costs[state.node, s.node, state.orientation, s.orientation] < Inf, possibles)
				bp = reduce((s1, s2) -> Costs[state.node, s1.node, state.orientation, s1.orientation] < Costs[state.node, s2.node, state.orientation, s2.orientation] ? s1 : s2, finite)
				push!(newtour, bp)
			end
			push!(newtour, p1[n])
			#newtour, _ = HillClimb(newtour, orientations, NodeCosts, Costs, supply)
			push!(nextpopulation, newtour)
			#nextpopulation[i] = newtour
		end
		# mutate
		for t in nextpopulation
			if rand(Float64) < mut
				#println(t)
				for i in 2:n-1
  				t[i].orientation = rand(oris)
				end
				shuffle!(t.states[2:end-1])
  			#t, _ = HillClimb(t, orientations, NodeCosts, Costs, supply)
				#println(t)
			end
		end
		# get new population
		append!(population, nextpopulation)
		for i in 1:populationsize
			ind = indmin(map(t -> tourCostVal(t), population))
			deleteat!(population, ind)
		end
		# calculate percentage
		firstval = Inf
		secondval = Inf
		for s in population
			v = tourCostVal(s)
			if v < firstval
				secondval = firstval
				firstval = v
				if v < bestcost
					bestcost = v
					besttour = deepcopy(s)
				end
			end
		end
		percent = firstval/secondval
		#println(percent)
		iters += 1
	end
	#println(iters, ", ", bestcost)
	#println(bestcost)
	return besttour, iters
end

function tourTransfer(R, ir, jr, i, j, e, dir)
	if dir == 0
		return R
	end
	Rij = deepcopy(R)
	Ri = Rij[ir]
	Rj = Rij[jr]
	if dir == 1
		insert!(Rj.states, j, State(Ri.states[i].node, e))
		deleteat!(Ri.states, i)
		if length(Ri) <= 2
			deleteat!(Rij, ir)
		end
	elseif dir == -1
		insert!(Ri.states, i, State(Rj.states[j].node, e))
		deleteat!(Rj.states, j)
		if length(Rj) <= 2
			deleteat!(Rij, jr)
		end
	end
	return Rij
end

function tourMerge(R, i, j, dir)
	Tij = Tour(State[], R[i].a)
	if dir == 1
		Tij.states = [R[i].states[1:end-1]; R[j].states[2:end]]
	else
		Tij.states = [R[j].states[1:end-1]; R[i].states[2:end]]
	end
	Rij = deepcopy(R)
	push!(Rij, Tij)
	deleteat!(Rij, i)
	deleteat!(Rij, j-1)
	return Rij
end

function bestTransferCost(R, ir, jr, orientations, NodeCosts, Costs, supply)
	Ri, Rj = R[ir], R[jr]
	mark = 0, 0, 0, 0
	variation = 0.0
	for i in 2:length(Ri)-1, j in 2:length(Rj)-1
		# Ri -> Rj
		varij, e = insertionCost(Rj, Ri[i].node, j, orientations, NodeCosts, Costs, supply)
		#println("i: ", varij)
		if length(Ri) <= 3
			varij -= tourCost(Ri, NodeCosts, Costs, supply)
		else
			varij += removalCost(Ri, i, NodeCosts, Costs, supply)
		end
		if -Inf < varij < variation
			variation = varij
			mark = i, j, e, 1
		end
		#println("ij: ", varij)
		# Rj -> Ri
		varji, e = insertionCost(Ri, Rj[j].node, i, orientations, NodeCosts, Costs, supply)
		if length(Rj) <= 3
			varji -= tourCost(Rj, NodeCosts, Costs, supply)
		else
			varji += removalCost(Rj, j, NodeCosts, Costs, supply)
		end
		if -Inf < varji < variation
			variation = varji
			mark = i, j, e, -1
		end
		#println("ij: ", varji)
	end
	return variation, mark
end

function updateFrequency(solution::Array{Tour, 1}, frequency::Dict{Int, Int})
	for tour in solution
		if !haskey(frequency, hash(tour))
			frequency[hash(tour)] = 0
		end
		frequency[hash(tour)] += 1
	end
	return frequency
end

function leastIncident(frequency::Dict{Int, Int}, tours::Dict{Int, Tour}, demandConstraint::Function)
	R = Tour[]
	#println(frequency)
	possibles = frequency
	while !demandConstraint(R)
		# println(length(possibles), ", ", demandConstraint(R))
		# println(possibles)
		#possibles = filter((tour, v) -> !(tour in R), incidence)
		least, val = first(possibles)
		for (tour, v) in possibles
			if v < val
				least = tour
				val = v
			end
		end
		#least = reduce((f1, f2) -> incidence[f1] >= incidence[f2] ? f2 : f1, keys(possibles))
		# println(least)
		# least = possibles[least_ind]
		# if all(r -> !hasintersection(r, least), R)
		# end
		push!(R, tours[least])
		#delete!(possibles, least)
		possibles = filter((r, num) -> !isequal(r, least), possibles)
	end
	return R
end



function worstSolution(startStates, endStates, orientations, NodeCosts, Costs)
	N = size(Costs, 1)
	num_agents = size(Costs, 5)

	R = Tour[]
	costR = 0.0
	for i in 1:N, a in 1:num_agents
		if i != startStates[a].node
			ec = 1
			cost = Costs[startStates[a].node, i, startStates[a].orientation, ec, a] +
			Costs[i, endStates[a].node, ec, endStates[a].orientation, a]
			for e in keys(orientations)
				c = Costs[startStates[a].node, i, startStates[a].orientation, e, a] +
				Costs[i, endStates[a].node, e, endStates[a].orientation, a]
				if c < cost < Inf
					cost = c
					ec = e
				end
			end
			if cost < Inf
				push!(R, Tour(State[startStates[a], State(i, ec), endStates[a]], a))
				costR += cost
			end
		end
	end

	return R, costR
end

function ARC_CiR(startStates::Dict{Int, AssignStage.State}, endStates::Dict{Int, AssignStage.State},
	orientations::Dict{Int, Float64}, NodeCosts, Costs, demand, supply, betas::Dict{Int, Float64};
	tourMaxRepeats=10, maxRepeats=10, printStep=100, maxARCIters=200)

	Hval(R) = H(R, NodeCosts, Costs, supply)
	capacityConstraintSol(R::Array{Tour}) = capacityConstraint(R, NodeCosts, Costs, betas, supply)
	demandConstraintSol(R::Array{Tour}) = demandConstraint(R, demand, supply)
	localoptim(r) = CiROptim(r, orientations, NodeCosts, Costs, supply, maxRepeats=tourMaxRepeats)

	N = size(Costs, 1)
	num_agents = size(Costs, 5)
	base_nodes = Dict(a => startStates[a].node for a in 1:num_agents)
	iters = 0
	repeat = 0
	optim_iters = 0
	frequency = Dict{Int, Int}()
	tours = Dict{Int, Tour}()
	R, valR = worstSolution(startStates, endStates, orientations, NodeCosts, Costs)
	for r in R
		tours[hash(r)] = r
	end
	# println(demandConstraintSol(R))
	# println(R)
	vals = Dict(hash(R) => valR)
	incidence = Dict(t => 0 for t in R)
	bestSol = R
	#op = ""
	while repeat < maxRepeats && iters < maxARCIters
		if rem(iters, printStep) == 0
			println("ARC Iter: ", iters, " Best Sol: ", vals[hash(bestSol)])
		end
		for i in eachindex(R)
			R[i], t = localoptim(R[i])
			tours[hash(R[i])] = R[i]
			optim_iters += t
		end
		vals[hash(R)] = Hval(R)
		frequency = updateFrequency(R, frequency)
		best_variation = 0.0
		best_R = R
		for i in 1:length(R) # goes to length beacuse of removal
			for j in i+1:length(R)
				if i != j && R[i].a == R[j].a
					variation, mark = bestTransferCost(R, i, j, orientations, NodeCosts, Costs, supply)
					if variation < best_variation
						# println("Transfer ", i, " to ", j, ": ", variation)
						Rij = tourTransfer(R, i, j, mark...)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							# parents[Rij] = R
							vals[hash(Rij)] = variation + vals[hash(R)]
							best_variation = variation
							best_R = Rij
							#op = string("transfer ", i, " to ", j, ", via ", mark, ": ", best_variation)
						end
					end
					variation, dir = mergeCost(R[i], R[j], Costs)
					if variation < best_variation
						# println("merge ", i, " to ", j, ": ", variation)
						Rij = tourMerge(R, i, j, dir)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							# parents[Rij] = R
							vals[hash(Rij)] = variation + vals[hash(R)]
							best_variation = variation
							best_R = Rij
							#op = string("merge ", i, " to ", j, ", ", best_variation)
						end
					end
				end
			end
			variation = -tourCost(R[i], NodeCosts, Costs, supply)
			if variation < best_variation
				# println("Delete ", i, ": ", variation)
				Ri = deleteat!(deepcopy(R), i)
				if demandConstraintSol(Ri)# && !tourInsideSet(Ri, visited)
					# parents[Ri] = R
					vals[hash(Ri)] = variation + vals[hash(R)]
					best_variation = variation
					best_R = Ri
					#op = string("delete ", i, ", ", best_variation)
				end
			end
			variation, mark = bestRemovalCost(R[i], NodeCosts, Costs, supply)
			if variation < best_variation
				Ri = removal(R, NodeCosts, Costs, i, mark)
				if demandConstraintSol(Ri)
					vals[hash(Ri)] = variation + vals[hash(R)]
					best_variation = variation
					best_R = Ri
					#op = string("delete node ", i, ", ", best_variation)
				end
			end
		end
		if best_variation < -1e-12
			#println(op)
			# println(Hval(R))
			R = best_R
			# println(Hval(R))
			# println(R)
		else
			if vals[hash(R)] < vals[hash(bestSol)]
				repeat = 0
				#println(repeat)
				# percent = vals[hash(bestSol)]/vals[hash(R)] - 1.0
				bestSol = deepcopy(R)
				vals[hash(bestSol)] = vals[hash(R)]
				# else
				# percent = vals[hash(R)]/vals[hash(bestSol)] - 1.0
			elseif equalTourArrays(R, bestSol)
				break
			end
			repeat += 1
			R_least = leastIncident(frequency, tours, demandConstraintSol)
			#println(repeat)
			#println(R)
			#println(R_least)
			if equalTourArrays(R, R_least)
				break
			else
				#println("Reseting at ", iters)
				R = R_least
				vals[hash(R)] = Hval(R)
			end
		end
		iters += 1
	end
	vals_dict = Dict{String, Float64}()
	for i in 1:length(bestSol)
		a = bestSol[i].a
		vals_dict["t $i a $a"] = tourCost(bestSol[i], NodeCosts, Costs, supply)
	end
	vals_dict["total"] = vals[hash(bestSol)]
	return bestSol, vals_dict, iters, optim_iters
end

function ARC_InO(startStates::Dict{Int, AssignStage.State}, endStates::Dict{Int, AssignStage.State},
	orientations::Dict{Int, Float64}, NodeCosts, Costs, demand, supply, betas::Dict{Int, Float64};
	tourMaxRepeats=10, maxRepeats=10, printStep=100)

	Hval(R) = H(R, NodeCosts, Costs, supply)
	capacityConstraintSol(R::Array{Tour}) = capacityConstraint(R, NodeCosts, Costs, betas, supply)
	demandConstraintSol(R::Array{Tour}) = demandConstraint(R, demand, supply)
	localoptim(r) = InOOptim(r, orientations, NodeCosts, Costs, supply, maxRepeats=tourMaxRepeats)

	N = size(Costs, 1)
	num_agents = size(Costs, 5)
	base_nodes = Dict(a => startStates[a].node for a in 1:num_agents)
	iters = 0
	repeat = 0
	optim_iters = 0
	frequency = Dict{Int, Int}()
	tours = Dict{Int, Tour}()
	R, valR = worstSolution(startStates, endStates, orientations, NodeCosts, Costs)
	for r in R
		tours[hash(r)] = r
	end
	# println(demandConstraintSol(R))
	# println(R)
	vals = Dict(hash(R) => valR)
	incidence = Dict(t => 0 for t in R)
	bestSol = R
	#op = ""
	while repeat < maxRepeats
		if rem(iters, printStep) == 0
			println("ARC Iter: ", iters, " Best Sol: ", vals[hash(bestSol)])
		end
		for i in eachindex(R)
			R[i], t = localoptim(R[i])
			tours[hash(R[i])] = R[i]
			optim_iters += t
		end
		vals[hash(R)] = Hval(R)
		frequency = updateFrequency(R, frequency)
		best_variation = 0.0
		best_R = R
		for i in 1:length(R) # goes to length beacuse of removal
			for j in i+1:length(R)
				if i != j && R[i].a == R[j].a
					variation, mark = bestTransferCost(R, i, j, orientations, NodeCosts, Costs, supply)
					if variation < best_variation
						# println("Transfer ", i, " to ", j, ": ", variation)
						Rij = tourTransfer(R, i, j, mark...)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							# parents[Rij] = R
							vals[hash(Rij)] = variation + vals[hash(R)]
							best_variation = variation
							best_R = Rij
							#op = string("transfer ", i, " to ", j, ", via ", mark, ": ", best_variation)
						end
					end
					variation, dir = mergeCost(R[i], R[j], Costs)
					if variation < best_variation
						# println("merge ", i, " to ", j, ": ", variation)
						Rij = tourMerge(R, i, j, dir)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							# parents[Rij] = R
							vals[hash(Rij)] = variation + vals[hash(R)]
							best_variation = variation
							best_R = Rij
							#op = string("merge ", i, " to ", j, ", ", best_variation)
						end
					end
				end
			end
			variation = -tourCost(R[i], NodeCosts, Costs, supply)
			if variation < best_variation
				# println("Delete ", i, ": ", variation)
				Ri = deleteat!(deepcopy(R), i)
				if demandConstraintSol(Ri)# && !tourInsideSet(Ri, visited)
					# parents[Ri] = R
					vals[hash(Ri)] = variation + vals[hash(R)]
					best_variation = variation
					best_R = Ri
					#op = string("delete ", i, ", ", best_variation)
				end
			end
			variation, mark = bestRemovalCost(R[i], NodeCosts, Costs, supply)
			if variation < best_variation
				Ri = removal(R, NodeCosts, Costs, i, mark)
				if demandConstraintSol(Ri)
					vals[hash(Ri)] = variation + vals[hash(R)]
					best_variation = variation
					best_R = Ri
					#op = string("delete node ", i, ", ", best_variation)
				end
			end
		end
		if best_variation < -1e-12
			#println(op)
			# println(Hval(R))
			R = best_R
			# println(Hval(R))
			# println(R)
		else
			if vals[hash(R)] < vals[hash(bestSol)]
				repeat = 0
				#println(repeat)
				# percent = vals[hash(bestSol)]/vals[hash(R)] - 1.0
				bestSol = deepcopy(R)
				vals[hash(bestSol)] = vals[hash(R)]
				# else
				# percent = vals[hash(R)]/vals[hash(bestSol)] - 1.0
			elseif equalTourArrays(R, bestSol)
				break
			end
			repeat += 1
			R_least = leastIncident(frequency, tours, demandConstraintSol)
			#println(repeat)
			#println(R)
			#println(R_least)
			if equalTourArrays(R, R_least)
				break
			else
				#println("Reseting at ", iters)
				R = R_least
				vals[hash(R)] = Hval(R)
			end
		end
		iters += 1
	end
	vals_dict = Dict{String, Float64}()
	for i in 1:length(bestSol)
		a = bestSol[i].a
		vals_dict["t $i a $a"] = tourCost(bestSol[i], NodeCosts, Costs, supply)
	end
	vals_dict["total"] = vals[hash(bestSol)]
	return bestSol, vals_dict, iters, optim_iters
end

function ARC_Gen(startStates::Dict{Int, AssignStage.State}, endStates::Dict{Int, AssignStage.State},
	orientations::Dict{Int, Float64}, NodeCosts, Costs, demand, supply, betas::Dict{Int, Float64};
	maxRepeats=10, maxPercent=0.95, populationsize=30, mut=0.4, printStep=100, maxGens=1000,
	maxARCIters=200)

	Hval(R) = H(R, NodeCosts, Costs, supply)
	capacityConstraintSol(R::Array{Tour}) = capacityConstraint(R, NodeCosts, Costs, betas, supply)
	demandConstraintSol(R::Array{Tour}) = demandConstraint(R, demand, supply)
	localoptim(r) = GenOptim(r, orientations, NodeCosts, Costs, supply,
		 maxPercent=maxPercent, populationsize=populationsize, mut=mut, maxGens=maxGens)

	N = size(Costs, 1)
	num_agents = size(Costs, 5)
	base_nodes = Dict(a => startStates[a].node for a in 1:num_agents)
	iters = 0
	repeat = 0
	optim_iters = 0
	frequency = Dict{Int, Int}()
	tours = Dict{Int, Tour}()
	R, valR = worstSolution(startStates, endStates, orientations, NodeCosts, Costs)
	valR = Hval(R)
	for r in R
		tours[hash(r)] = r
	end
	# println(demandConstraintSol(R))
	# println(R)
	vals = Dict(hash(R) => valR)
	incidence = Dict(t => 0 for t in R)
	bestSol = R
	#op = ""
	while repeat < maxRepeats && iters < maxARCIters
		if rem(iters, printStep) == 0
			println("ARC Iter: ", iters, " OPTIM Iter: ", optim_iters, " Best Sol: ", vals[hash(bestSol)])
		end
		for i in eachindex(R)
			R[i], t = localoptim(R[i])
			tours[hash(R[i])] = R[i]
			optim_iters += t
		end
		vals[hash(R)] = Hval(R)
		frequency = updateFrequency(R, frequency)
		best_variation = 0.0
		best_R = R
		for i in 1:length(R) # goes to length beacuse of removal
			for j in i+1:length(R)
				if i != j && R[i].a == R[j].a
					variation, mark = bestTransferCost(R, i, j, orientations, NodeCosts, Costs, supply)
					if variation < best_variation
						# println("Transfer ", i, " to ", j, ": ", variation)
						Rij = tourTransfer(R, i, j, mark...)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							# parents[Rij] = R
							vals[hash(Rij)] = variation + vals[hash(R)]
							best_variation = variation
							best_R = Rij
							#op = string("transfer ", i, " to ", j, ", via ", mark, ": ", best_variation)
						end
					end
					variation, dir = mergeCost(R[i], R[j], Costs)
					if variation < best_variation
						# println("merge ", i, " to ", j, ": ", variation)
						Rij = tourMerge(R, i, j, dir)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							# parents[Rij] = R
							vals[hash(Rij)] = variation + vals[hash(R)]
							best_variation = variation
							best_R = Rij
							#op = string("merge ", i, " to ", j, ", ", best_variation)
						end
					end
				end
			end
			variation = -tourCost(R[i], NodeCosts, Costs, supply)
			if variation < best_variation
				# println("Delete ", i, ": ", variation)
				Ri = deleteat!(deepcopy(R), i)
				if demandConstraintSol(Ri)# && !tourInsideSet(Ri, visited)
					# parents[Ri] = R
					vals[hash(Ri)] = variation + vals[hash(R)]
					best_variation = variation
					best_R = Ri
					#op = string("delete ", i, ", ", best_variation)
				end
			end
			variation, mark = bestRemovalCost(R[i], NodeCosts, Costs, supply)
			if variation < best_variation
				Ri = removal(R, NodeCosts, Costs, i, mark)
				if demandConstraintSol(Ri)
					vals[hash(Ri)] = variation + vals[hash(R)]
					best_variation = variation
					best_R = Ri
					#op = string("delete node ", i, ", ", best_variation)
				end
			end
		end
		if best_variation < 0.0
			#println(op)
			# println(Hval(R))
			R = best_R
			# println(Hval(R))
			# println(R)
		else
			if vals[hash(R)] < vals[hash(bestSol)]
				repeat = 0
				#println(repeat)
				# percent = vals[hash(bestSol)]/vals[hash(R)] - 1.0
				bestSol = deepcopy(R)
				vals[hash(bestSol)] = vals[hash(R)]
				# else
				# percent = vals[hash(R)]/vals[hash(bestSol)] - 1.0
			elseif equalTourArrays(R, bestSol)
				break
			end
			repeat += 1
			R_least = leastIncident(frequency, tours, demandConstraintSol)
			#println(repeat)
			#println(R)
			#println(R_least)
			if equalTourArrays(R, R_least)
				break
			else
				#println("Reseting at ", iters)
				R = R_least
				vals[hash(R)] = Hval(R)
			end
		end
		iters += 1
	end
	vals_dict = Dict{String, Float64}()
	for i in 1:length(bestSol)
		a = bestSol[i].a
		vals_dict["t $i a $a"] = tourCost(bestSol[i], NodeCosts, Costs, supply)
	end
	vals_dict["total"] = vals[hash(bestSol)]
	return bestSol, vals_dict, iters, optim_iters
end


function capacityConstraint(tours::Array{Tour, 1}, NodeCosts, Costs, betas, supply)
	for tour in tours
		if tourCost(tour, NodeCosts, Costs, supply) > betas[tour.a]
			return false
		end
	end
	return true
	# feasible = true
	# for tour in tours
	#   if tourCost(tour, NodeCosts, DirectCosts, TurnCosts) > beta_val
	#     feasible = false
	#   end
	# end
	#return all(tourCost(tour, NodeCosts, Costs) < beta_val for tour in tours)
end

function demandConstraint(tours::Array{Tour}, demand, supply)
	num_operations = size(demand, 2)
	num_nodes = size(demand, 1)
	supplied = fill(false, (num_nodes, num_operations))
	for op in 1:num_operations
		for r in tours
			for i in r
				supplied[i.node, op] = supplied[i.node, op] || supply[r.a, op] #+= supply[r.a, op]
			end
		end
	end
	return all(supplied .| .!demand)#any(.!(supplied .> demand))
end

function bestScore(frequency::Dict{Int, Int}, Costs::Array{Float64,5}, tours::Dict{Int, Tour}, demandConstraint::Function)
	R = Tour[]
	possibles = frequency
	best, val = first(possibles)
	for (tour, v) in possibles
		if v < val
			best = tour
			val = v
		end
	end
	push!(R, tours[best])
	possibles = filter((r, num) -> !isequal(r, best), possibles)
	while !demandConstraint(R)
		bestscore = 0.0
		best = 1
		for (id, freq) in possibles
			score = sum(Costs[ri.node, rj.node, ri.orientation, rj.orientation, tours[id].a] for ri in tours[id][2:end-1], rj in tours[id][2:end-1])
			for r in R
				a = r.a
				score += sum(Costs[ri.node, ti.node, ri.orientation, ti.orientation, a] for ri in r[2:end-1], ti in tours[id][2:end-1])
				score += sum(Costs[ti.node, ri.node, ti.orientation, ri.orientation, a] for ri in r[2:end-1], ti in tours[id][2:end-1])
				score += score/freq
			end
			if score > bestscore
				bestscore = score
				best = id
			end
		end
		push!(R, tours[best])
		possibles = filter((r, num) -> !isequal(r, best), possibles)
	end
	return R
end

function OOSOptim(tour::Tour, orientations::Dict{Int, Float64}, NodeCosts::Array{Float64, 2}, Costs::Array{Float64, 5}, supply; maxRepeats=10, maxIters=100)
	n = length(tour)
	if n <= 3
		return tour, 0.0, 0
	end
	m = length(orientations)
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	inittour = deepcopy(tour)
	initcost = tourCostVal(inittour)
	bestcost = initcost
	cost = initcost
	besttour = deepcopy(tour)
	iters = 1
	repeat = 0
	weights = Dict{Tuple{Int,Int,Int},Float64}()

	getWeight(si::Int, sj::Int, o::Int) = haskey(weights, (si, sj, o)) ? weights[(si, sj, o)] : 1.0
	function upWeight(si::Int, sj::Int, o::Int, val::Number)
		if haskey(weights, (si, sj, o))
			 weights[(si, sj, o)] += val
		else
			 weights[(si, sj, o)] = 1.0 + val
		end
	end

	while repeat < maxRepeats && iters < maxIters
		#incidence = updateIncidence(tour, incidence)
		choicei, choicej, b, f, op = 0, 0, 0, 0, 0
		score = 0.0
		variation = 0.0
		for i in 2:length(tour)-1
			for j in i+1:length(tour)-1
				c, orib, orif = switchCost(tour, i, j, orientations, Costs)
				s = c/getWeight(i, j, 1)
				if s < score
					score = s
					variation = c
					choicei, choicej, b, f, op = i, j, orib, orif, 1
				end
			end
			c, ori = reorientationCost(tour, i, orientations, Costs)
			s = c/getWeight(i, i, 2)
			if s < score
				score = s
				variation = c
				choicei, b, op = i, ori, 2
			end
		end
		cost += variation
		if op == 1
			upWeight(choicei, choicej, 1, 1)
			tour[choicei].orientation = f
			tour[choicej].orientation = b
			tour[choicei], tour[choicej] = tour[choicej], tour[choicei]
		elseif op == 2
			upWeight(choicei, choicei, 2, 1)
			tour[choicei].orientation = b
		else
			if cost < bestcost
				repeat = 0
				bestcost = cost
				besttour = deepcopy(tour)
			end
			repeat += 1
			shuffle!(tour[2:end-1])
			for i in 2:length(tour)-1
				tour[i].orientation = rand(1:m)
			end
			cost = tourCostVal(tour)
		end
		for (k, v) in weights
			weights[k] = 0.95*v
		end
		iters += 1
	end
	return besttour, bestcost - initcost, iters
end

function ReSCOptim(tour::Tour, orientations::Dict{Int, Float64}, NodeCosts::Array{Float64, 2}, Costs::Array{Float64, 5}, supply; maxRepeats=10, maxIters=100)
	n = length(tour)
	if n <= 3
		return tour, 0.0, 0
	end
	m = length(orientations)
	tourCostVal(tour) = tourCost(tour, NodeCosts, Costs, supply)
	bestcost = tourCostVal(tour)
	initcost = tourCostVal(tour)
	besttour = deepcopy(tour)
	inittour = deepcopy(tour)
	iters = 1
	repeat = 0
	weights = Dict{Tuple{Int,Int,Int,Int,Int},Float64}()

	getWeight(ni::Int, oi::Int, nj::Int, oj::Int, pos::Int) = haskey(weights, (ni, oi, nj, oj, pos)) ? weights[(ni, oi, nj, oj, pos)] : 1.0
	function upWeight(ni::Int, oi::Int, nj::Int, oj::Int, pos::Int, val::Number)
		if haskey(weights, (ni, oi, nj, oj, pos))
			 weights[(ni, oi, nj, oj, pos)] += val
		else
			 weights[(ni, oi, nj, oj, pos)] = 1.0 + val
		end
	end
	
	while repeat < maxRepeats && iters < maxIters
		score = Inf
		bestc = Inf
		bestj, bestphij = 1, 1
		best_op = 1, 1, 1, 1, 1
		for i in 2:length(tour)-2
			phii = tour[i].orientation
			ni = tour[i].node
			for j in i+1:length(tour)-1, phij in 1:m
				nj = tour[j].node
				c = Costs[ni, nj, phii, phij, tour.a]
  			s = c*getWeight(ni, phii, nj, phij, i+1)
				if s < score
					bestj = j
					bestphij = phij
					bestc = c
					score = s
					#upWeight(ni, phii, nj, phij, i+1, 2)
      		best_op = ni, phii, nj, phij, i+1
				end
				upWeight(best_op..., 1)
			end
			tour[i+1], tour[bestj] = tour[bestj], tour[i+1]
			tour[i+1].orientation = bestphij
		end
		#for i in 2:length(tour)-1
			#c = Inf
			#bestphi = 1
			#ni = tour[i].node
			#phii = tour[i].orientation
			#for phi in 1:m
				#cost = Costs[tour[i-1].node, tour[i].node, tour[i-1].orientation, phi, tour.a] +
      				 #Costs[tour[i].node, tour[i+1].node, phi, tour[i+1].orientation, tour.a] 
				#if cost < c
					#bestphi = phi
					#c = cost
				#end
			#end
			#tour[i].orientation = bestphi
		#end
		if tourCostVal(tour) < tourCostVal(besttour)
			repeat = 0
			bestcost = tourCostVal(tour)
			besttour = deepcopy(tour)
		end
		repeat += 1
		iters += 1
		for (k, v) in weights
			weights[k] = 0.95*v
		end
		shuffle!(tour.states[2:n-1])
		#for i in 2:n-1
			#tour[i].orientation = rand(1:m)
		#end
		tour[2].orientation = rand(1:m)
		#change = rand(3:n-1)
		#tour[change].orientation = rand(1:m)
		#tour[change], tour[2] = tour[2], tour[change]
		#tour = deepcopy(inittour)
	end
	return besttour, bestcost - initcost, iters
end

function ARC(startStates::Dict{Int, AssignStage.State}, endStates::Dict{Int, AssignStage.State},
	orientations::Dict{Int, Float64}, NodeCosts::Array{Float64, 2}, Costs::Array{Float64, 5}, demand, supply, betas::Dict{Int, Float64},
	localOptim=OOSOptim, localOptimOpts...; maxARCRepeats=10, maxARCIters=100, maxRepeats=20, maxIters=150,
  printStep=100)

	Hval(R) = H(R, NodeCosts, Costs, supply)
	capacityConstraintSol(R::Array{Tour}) = capacityConstraint(R, NodeCosts, Costs, betas, supply)
	demandConstraintSol(R::Array{Tour}) = demandConstraint(R, demand, supply)

	N = size(Costs, 1)
	num_agents = size(Costs, 5)
	base_nodes = Dict(a => startStates[a].node for a in 1:num_agents)
	iters = 0
	repeat = 0
	optim_iters = 0
	tours = Dict{Int, Tour}()

	R, valR = worstSolution(startStates, endStates, orientations, NodeCosts, Costs)
	worstSol = deepcopy(R)
	weights = Dict{Tuple{Int,Int,Int},Float64}()

	getWeight(Ri::Tour, Rj::Tour, o) = haskey(weights, (hash(Ri), hash(Rj), o)) ? weights[(hash(Ri), hash(Rj), o)] : 1.0
	getWeight(Ri::Int, Rj::Int, o) = haskey(weights, (Ri, Rj, o)) ? weights[(Ri, Rj, o)] : 1.0
	function upWeight(Ri::Tour, Rj::Tour, o, val)
		if haskey(weights, (Ri, Rj, o))
			 weights[(hash(Ri), hash(Rj), o)] += val
		else
			 weights[(hash(Ri), hash(Rj), o)] = 1.0 + val
		end
	end

	bestSol = R
	valR = Hval(R)
	valBestSol = Hval(bestSol)
	best_op = 1, 1, 1
	for r in R
		tours[hash(r)] = r
	end

	while repeat < maxARCRepeats && iters < maxARCIters
		if rem(iters, printStep) == 0
			println("ARC Iter: ", iters, " OPTIM Iter: ", optim_iters, " Best Sol: ", Hval(bestSol))
		end
		for i in eachindex(R)
			#R[i] = haskey(tours, hash(R[i])) ? tours[hash(R[i])] : R[i]
			R[i], variation, t = localOptim(R[i], orientations, NodeCosts, Costs, supply, localOptimOpts..., maxRepeats=maxRepeats, maxIters=maxIters)
			valR += variation
			tours[hash(R[i])] = R[i]
			optim_iters += t
		end
		#vals[hash(R)] = Hval(R)
		#frequency = updateFrequency(R, frequency)
		best_score = 0.0
		best_variation = 0.0
		best_R = R
		for i in 1:length(R) # goes to length beacuse of removal
			for j in i+1:length(R)
				if i != j && R[i].a == R[j].a
					variation, mark = bestTransferCost(R, i, j, orientations, NodeCosts, Costs, supply)
					score = variation/getWeight(R[i], R[j], 1)
					if score < best_score 
						Rij = tourTransfer(R, i, j, mark...)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							#vals[hash(Rij)] = variation + vals[hash(R)]
							#upWeight(R[i], R[j], 1, 2)
    					best_op = R[i], R[j], 2
							best_score = score 
							best_variation = variation 
							best_R = Rij
						end
					end
					variation, dir = mergeCost(R[i], R[j], Costs)
					score = variation/getWeight(R[i], R[j], 2)
					if score < best_score 
						Rij = tourMerge(R, i, j, dir)
						if capacityConstraintSol(Rij)# && !tourInsideSet(Rij, visited)
							#vals[hash(Rij)] = variation + vals[hash(R)]
							#upWeight(R[i], R[j], 2, 2)
    					best_op = R[i], R[j], 2
							best_score = score
							best_variation = variation 
							best_R = Rij
						end
					end
				end
			end
			variation = -tourCost(R[i], NodeCosts, Costs, supply)
			score = variation/getWeight(R[i], R[i], 3)
			if score < best_score
				Ri = deleteat!(deepcopy(R), i)
				if demandConstraintSol(Ri)
					#vals[hash(Ri)] = variation + vals[hash(R)]
					#upWeight(R[i], R[i], 3, 2)
					best_op = R[i], R[i], 3
					best_score = score
					best_variation = variation 
					best_R = Ri
				end
			end
			variation, mark = bestRemovalCost(R[i], NodeCosts, Costs, supply)
			score = variation/getWeight(R[i], R[i], 4)
			if score < best_score
				Ri = removal(R, NodeCosts, Costs, i, mark)
				if demandConstraintSol(Ri)
					#vals[hash(Ri)] = variation + vals[hash(R)]
					#upWeight(R[i], R[i], 4, 2)
					best_op = R[i], R[i], 4
					best_score = score
					best_variation = variation 
					best_R = Ri
				end
			end
		end
		if best_score < -1e-10
			R = best_R
  		#valR = Hval(R)
			valR += best_variation
			upWeight(best_op[1], best_op[2], best_op[3], 1)
		else
			if valR < valBestSol
				repeat = 0
				bestSol = deepcopy(R)
				valBestSol = valR
				#vals[hash(bestSol)] = vals[hash(R)]
			end
			R = Tour[]
			while !demandConstraintSol(R)
				idr, r = rand(tours)
				if !(r in R)
					push!(R, r)
				end
			end
			valR = Hval(R)
			repeat += 1
			#vals[hash(R)] = Hval(R)
		end
		for (k, v) in weights
			weights[k] = v*0.98
		end
		iters += 1
	end
	vals_dict = Dict{String, Float64}()
	for i in 1:length(bestSol)
		a = bestSol[i].a
		vals_dict["t$i,a$a"] = tourCost(bestSol[i], NodeCosts, Costs, supply)
	end
	vals_dict["total"] = Hval(bestSol)
	return bestSol, vals_dict, iters, optim_iters
	
end

function bestSeed(R::Array{Tour,1}, Costs::Array{Float64,5}, supplied)

	nodes = size(Costs,1)
	oris = size(Costs,3)
	agents = size(Costs,3)
	
	v = Inf
	nodeseed = 1
	agentseed = 1
	for n in 1:nodes
		if !supplied[n]
			for j in 1:nodes, phij in 1:oris, phin in 1:oris, a in 1:agents
  			if Costs[j, phij, n, phin, a] < v
					v = Costs[j, phij, n, phin, a]
					nodeseed = n
					agentseed = n
				end
			end
		end
	end
	
	return nodeseed, agentseed

end

function updateSupplied(R::Array{Tour,1}, supplied::Array{Bool, 1}, demand, supply::Array{Float64,2})
	ops = size(demand, 2)
	for r in R, n in r
		sup = true
		for o in 1:ops
			sup = sup && (!demand[n.node, o] || supply[tour.a, o] > 0)
		end
		supplied[n.node] = supplied[n.node] || sup
	end
	return supplied
end

function updateSupplied(tour::Tour, supplied::Array{Bool, 1}, demand, supply::Array{Float64,2})
	ops = size(demand, 2)
	for n in tour
		sup = true
		for o in 1:ops
			sup = sup && (!demand[n.node, o] || supply[tour.a, o] > 0)
		end
		supplied[n.node] = supplied[n.node] || sup
	end
	return supplied
end

function bestInsertion(tour::Tour, node::Int, NodeCosts::Array{Float64,2}, Costs::Array{Float64, 5}, supply::Array{Float64,2})
	oris = size(Costs, 3)
	v = Inf
	bestind = 2
	bestori = 1
	for i in 2:length(tour)-1
		sb = tour[i]
		s = tour[i+1]
		for ori in 1:oris
			if Costs[sb.node, node, sb.orientation, ori, tour.a] + Costs[s.node, node, s.orientation, ori, tour.a] < v
				v = Costs[sb.node, node, sb.orientation, ori, tour.a] + Costs[s.node, node, s.orientation, ori, tour.a] 
				bestori = ori
			end
		end
	end
	v += sum(NodeCosts[bestind, o]*supply[tour.a, o] for o in 1:size(NodeCosts, 2))
	return v, bestind, bestori
end

function ARC2(startStates::Dict{Int, AssignStage.State}, endStates::Dict{Int, AssignStage.State},
	orientations::Dict{Int, Float64}, NodeCosts::Array{Float64, 2}, Costs::Array{Float64, 5}, demand, supply, betas::Dict{Int, Float64},
	maxRepeats=10, maxARCIters=100, localOptim=InOOptim, localOptimOpts...;
  printStep=100)

	Hval(R) = H(R, NodeCosts, Costs, supply)
	capacityConstraintSol(R::Array{Tour}) = capacityConstraint(R, NodeCosts, Costs, betas, supply)
	demandConstraintSol(R::Array{Tour}) = demandConstraint(R, demand, supply)
	tourCostVal(t::Tour) = tourCost(t, NodeCosts, Costs, supply)

	N = size(Costs, 1)
	M = size(Costs, 3)
	num_agents = size(Costs, 5)
	base_nodes = Dict(a => startStates[a].node for a in 1:num_agents)
	iters = 0
	repeat = 0
	optim_iters = 0
	tours = Dict{Int, Tour}()
	bestSol = Tour[]
	supplied = fill(false, N)
	#valR = Hval(R)
	#vals = Dict(hash(bestSol) => valR)
	#for r in R
		#tours[hash(r)] = r
	#end
	while iters < maxARCIters
		R = Tour[]
		while !demandConstraintSol(R)
  		if rem(iters, printStep) == 0
  			println("ARC Iter: ", iters, " OPTIM Iter: ", optim_iters)
  		end
			# --- seed
    	nodeseed, oriseed, agentseed = 1, 1, 1
			val = Inf
			for i in 1:N, phi in 1:M, a in 1:num_agents
				st = startStates[a]
				se = endStates[a]
				if !supplied[i] && Costs[st.node, i, st.orientation, phi, a] + Costs[se.node, i, se.orientation, phi, a] < val
					val = Costs[st.node, i, st.orientation, phi, a] + Costs[se.node, i, se.orientation, phi, a] 
					nodeseed, oriseed, agentseed = i, phi, a
				end
			end
  		r = Tour([startStates[agentseed], State(nodeseed, oriseed), endStates[agentseed]], agentseed)
  		#inssertval, i, phi = bestInsertion(R[1], nodeseed, Costs)
  		#insertat!(R[1], i, State(nodeseed, phi))
			# --- growth
			val = tourCostVal(r)
			full = false
			while !full
				sleep(1)
				println(r)
  			supplied = updateSupplied(r, supplied, demand, supply)
				bestvaladd, bestnode, bestpos, bestori = Inf, 1, 1, 1
  			for n in 1:N
					if !supplied[n]
  					valadd, pos, ori = bestInsertion(r, n, NodeCosts, Costs, supply)
						if valadd < bestvaladd
							valadd = bestvaladd
							bestnode = n
							bestpos = pos
							bestori = ori
						end
					end
  			end
				if val + bestvaladd < betas[agentseed]
  				r.states = insert!(r.states, bestpos, State(bestnode, bestori))
					val += bestvaladd
				else
					push!(R, r)
					full = true
				end
    		iters += 1
			end
  	end
		bestSol = R
	end
	vals_dict = Dict{String, Float64}()
	for i in 1:length(bestSol)
		a = bestSol[i].a
		vals_dict["t$i,a$a"] = tourCost(bestSol[i], NodeCosts, Costs, supply)
	end
	vals_dict["total"] = Hval(bestSol)
	return bestSol, vals_dict, iters, optim_iters
end

function MILP(startStates::Dict{Int, AssignStage.State}, endStates::Dict{Int, AssignStage.State},
	orientations::Dict{Int, Float64}, NodeCosts, Costs, demand, supply, betas::Dict{Int, Float64},
	solver)

	nodes = size(Costs, 1)
	dirs = size(Costs, 3)
	agents = size(Costs, 5)
	operations = size(demand, 2)
	base_node = startStates[1].node

	cadcop = Model(solver=solver)

	@variable(cadcop, 0 <= x[1:nodes, 1:nodes, 1:dirs, 1:dirs, 1:agents] <= nodes, Int)
	@variable(cadcop, y[1:nodes, 1:nodes, 1:dirs, 1:dirs, 1:agents], Bin)
	

	@objective(cadcop, Min,
		sum(Costs[i,j,phii,phij,a]*x[i,j,phii,phij,a] for i in 1:nodes, j in 1:nodes, phii in 1:dirs, phij in 1:dirs, a in 1:agents
		if Costs[i,j,phii,phij,a] < Inf)
	)
	
	for i in 1:nodes, j in 1:nodes, phii in 1:dirs, phij in 1:dirs, a in 1:agents
		@constraint(cadcop, nodes*y[i, j, phii, phij, a] >= x[i,j,phii,phij,a])
	end

	for i in 1:nodes, j in 1:nodes, phii in 1:dirs, phij in 1:dirs, a in 1:agents
		if Costs[i,j,phii,phij,a] == Inf
			@constraint(cadcop, x[i,j,phii,phij,a] == 0)
		end
	end

	for j in 1:nodes, phij in 1:dirs, a in 1:agents
		if j != startStates[a].node && j != endStates[a].node
			@constraint(cadcop,
			sum(x[i,j,phii,phij,a] for i in 1:nodes, phii in 1:dirs) - sum(x[j,k,phij,phik,a] for k in 1:nodes, phik in 1:dirs) == 0
			)
		end
	end

	for a in 1:agents
		@constraint(cadcop,
		sum(x[startStates[a].node,j,startStates[a].orientation,phij,a] for j in 1:nodes, phij in 1:dirs) -
		sum(x[j,endStates[a].node,phij,endStates[a].orientation,a] for j in 1:nodes, phij in 1:dirs) == 0
		)
	end

	for a in 1:agents
		@constraint(cadcop,
		sum(x[j,endStates[a].node,phij,endStates[a].orientation,a] for j in 1:nodes, phij in 1:dirs) -
		sum(x[startStates[a].node,j,startStates[a].orientation,phij,a] for j in 1:nodes, phij in 1:dirs) == 0
		)
	end

	#for a in 1:agents
	#@constraint(cadcop,
	#sum(x[startStates[a].node, k, startStates[a].orientation, phik, a] for k in 1:nodes, phik in 1:dirs) >= 1
	#)
	#end

	for j in 1:nodes, o in 1:operations
		@constraint(cadcop,
  		sum(supply[a, o]*x[i, j, phii, phij, a] for i in 1:nodes, phii in 1:dirs, phij in 1:dirs, a in 1:agents)
			>= demand[j, o]
		)
	end

	for i in 1:nodes, phii in 1:dirs, phij in 1:dirs, a in 1:agents
		if i != startStates[a].node && i != endStates[a].node
			@constraint(cadcop, x[i,i,phii,phij,a] == 0 )
		end
		if phii != startStates[a].orientation
			@constraint(cadcop, x[startStates[a].node, i, phii, phij, a] == 0 )
		end
		if phij != endStates[a].orientation
			@constraint(cadcop, x[i, endStates[a].node, phii, phij, a] == 0 )
		end
	end

	function findtour(X)
		tour = Array{Int, 1}()
		tourdirs = Array{Int, 1}()
		found = false
		i, j, k, phii, phij, phik, a = 1, 1, 1, 1, 1, 1, 1
		for j in 1:nodes, phij in 1:dirs, phii in 1:dirs, a in 1:agents
			if X[base_node, j, phii, phij, a] >= 1 - 1e-3
				found = true
				i = base_node
				break
			end
		end
		if !found
			for i in 1:nodes, j in 1:nodes, phii in 1:dirs, phij in 1:dirs, a in 1:agents
				if X[i,j,phii,phij,a] >= 1 - 1e-3
					found = true
					break
				end
			end
		end
		if found
  		#println("start i,",i,",j,",j," phii ",phii," phij ",phij," a ",a," X ", X[i,j,phii,phij,a])
			push!(tour, i)
			push!(tourdirs, phii)
			while !(j in tour) && found
				found = false
				push!(tour, j)
				push!(tourdirs, phij)
				#println(tour)
				for k in 1:nodes, phik in 1:dirs
					#println("j ", j, " k ", k, " phik ", phik, " phij ", phij, " X ", X[j,k,phij,phik,a])
					if X[j, k, phij, phik, a] >= 1 - 1e-3
						found = true
						j = k
						phij = phik
						break
					end
				end
			end
			push!(tour, j)
			push!(tourdirs, phij)
			init = findfirst(tour, j)
			for i in init:length(tour)-1
				X[tour[i], tour[i+1], tourdirs[i], tourdirs[i+1], a] -= 1
			end
			return tour[init:end], tourdirs[init:end], a
		end
		return tour, tourdirs, a
	end

	function lazyConstraint(cb)
		X = getvalue(x)
		tour, tourdirs, a = findtour(X)
		#println("res: $tour")
		while length(tour) > 2
			#println(a, tour)
			# take out last added node to build restriction
			#tour = tour[1:end-1]
			#println("Adding tour", tour, " with base", startStates[a].node)
			if !(startStates[a].node in tour)
				t = tour[1:end-1]
				#@lazyconstraint(cb,
				#sum(x[ti,tj,phii,phij,a] for ti in t, tj in t, phii in 1:dirs, phij in 1:dirs) <= length(t) - 1
				#)
				@lazyconstraint(cb,
				sum(y[ti,tj,phii,phij,a] for ti in t, tj in t, phii in 1:dirs, phij in 1:dirs) <= length(t) - 1
				)
				#outs = filter(n -> !(n in tour), 1:nodes)
				#@lazyconstraint(cb,
					#sum(x[tout, tin, phiout, phiin, a] for tout in outs, tin in tour, phiout in 1:dirs, phiin in 1:dirs) >= 1
				#)
			end
			#Y = getvalue(y)
			total = sum(NodeCosts[ti, o]*supply[a, o] for ti in tour, o in 1:size(supply, 2)) +
			sum(Costs[tour[i],tour[i+1],tourdirs[i],tourdirs[i+1],a] for i in 1:length(tour)-1)
			#println(betas[a])
			if total > betas[a]
				#println("over beta ", a, tour)
				#@lazyconstraint(cb,
  				#sum(x[tour[i],tour[i+1],tourdirs[i],tourdirs[i+1],a] for i in 1:length(tour)-1) <= length(tour)-2
				#)
				@lazyconstraint(cb,
  				sum(y[tour[i],tour[i+1],tourdirs[i],tourdirs[i+1],a] for i in 1:length(tour)-1) <= length(tour)-2
				)
			end
			#@lazyconstraint(cb,
				#sum(NodeCosts[ti, o]*supply[a, o] for ti in tour, o in 1:size(supply, 2)) +
  			#sum(Costs[tour[i],tour[i+1],tourdirs[i],tourdirs[i+1],a]*x[tour[i],tour[i+1],tourdirs[i],tourdirs[i+1],a] for i in 1:length(tour)-1) <= betas[a]
			#)
			#println(tour, ". ", tourdirs)
			tour, tourdirs, a = findtour(X)
		end
	end

	addlazycallback(cadcop, lazyConstraint)

	status = solve(cadcop)

	solution = Array{ImplementationTypes.Tour, 1}()
	
	X = getvalue(x)
	#println("sol")
	tour, tourdirs, a = findtour(X)
	while length(tour) > 0
		println(a, tour)
		toursol = Tour(Array{ImplementationTypes.State, 1}(), a)
		for i in 1:length(tour)-1
			push!(toursol.states, ImplementationTypes.State(tour[i], tourdirs[i]))
		end
		push!(toursol, ImplementationTypes.State(tour[end], tourdirs[end]))
		push!(solution, toursol)
		tour, tourdirs, a = findtour(X)
	end

	vals_dict = Dict("t$i" => tourCost(solution[i], NodeCosts, Costs, supply) for i in 1:length(solution))
	vals_dict["total"] = H(solution, NodeCosts, Costs, supply)
	return solution, vals_dict, 0, 0

end

	# DEPRECATED
	@deprecate(updateScore, updateIncidence)
	function updateScore(node, parents, score::Dict{Tour, Float64})
		curnode = node
		println(tour)
		while haskey(parents, curnode)
			for r in curnode
				if !haskey(score, curnode)
					score[r] = 0.0
				end
				score[r] += 1.0
			end
			curnode = parents[curnode]
		end
	end

	function treeLevel(node, parents)
		if haskey(parents, node)
			return 1 + treeLevel(parents[node], parents)
		else
			return 1
		end
	end

end

function tourCost(tour, a, NodeCosts, DirectCosts, TurnCosts)
	# must start and end in the base_n
	cost = 0.0
	# n = size(tour.nodes, 1)
	nops = size(NodeCosts, 2)
	# println(DirectCosts)
	simm = start(tour)
	imm, sim = next(tour, simm)
	im, si = next(tour, sim)
	# cost += sum(NodeCosts[im, :])
	cost += DirectCosts[imm, im, a]
	while !done(tour, si)
		imm, simm = next(tour, simm)
		im, sim = next(tour, sim)
		i, si = next(tour, si)
		cost += sum(NodeCosts[im, :])
		cost += DirectCosts[im, i, a]
		cost += TurnCosts[imm, im, i, a]
	end
	return cost
end
