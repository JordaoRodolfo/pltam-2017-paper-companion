#!/usr/bin/env julia
if !(pwd() in LOAD_PATH)
    push!(LOAD_PATH, pwd())
end

using ImplementationTypes
using BuildStage
using AssignStage
using PGFPlots
using ColorTypes
using ColorBrewer
#using Plots
using ArgParse
using YAML
using Utils
#using Cbc
#solver = CbcSolver()

solver = nothing
try
  using Gurobi
  solver = GurobiSolver()
catch 
  using GLPKMathProgInterface
  solver = GLPKSolverMIP()
end


#gr()
#pgfplots()
#pyplot()
#rc("text", usetex=true)
#rc("text.latex", unicode=true)
#plotlyjs()

s = ArgParseSettings()

@add_arg_table s begin
	"--non-grid"
		help = "Generate instance in a grid, instead of free."
		action = :store_true
	"--plot-all"
		help = "Plot all intermediate steps"
		action = :store_true
	"--exact"
		help = "Solve via MILP"
		action = :store_true
	"--localMethod"
		help = "Local Method optim. to use. (Default: ReSS)"
		arg_type = String
		default = "InO"
	"Specs"
		help = "Name of specs file for generation"
		arg_type = String
		default = "situation7-specs.yaml"
	"-l", "--label"
		help = "Label given to files of output"
		arg_type = String
		default = "results"
end

args = parse_args(s)

println("Reading specs...")

output_name = args["label"]
specs = YAML.load(open(args["Specs"]))

buildDict = @time buildStage(specs, verbose=true)

println("Building model...")

assignDict = @time buildModel(buildDict)

println("Solving...")

#sol, val, iters, optim_iters = @time ARC2(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
            	#20, 200, CiROptim, 300, printStep=1)
if !args["exact"]
	if args["localMethod"] == "CiR"
    sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                	20, 200, CiROptim, 300, printStep=5)
	elseif args["localMethod"] == "InO"
    sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                	20, 200, InOOptim, 300, printStep=5)
	elseif args["localMethod"] == "OOS"
    sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                	OOSOptim, maxARCIters=300, maxARCRepeats=10, maxRepeats=10, maxIters=100, printStep=20)
	elseif args["localMethod"] == "ReSC"
    sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                	ReSCOptim, maxARCIters=300, maxARCRepeats=10, maxRepeats=10, maxIters=100, printStep=20)
	elseif args["localMethod"] == "Gen"
    sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
    							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                	GenOptim, maxARCIters=1000, maxARCRepeats=10, maxRepeats=20, maxIters=2000, printStep=20)
	end
else
  sol, val, iters, optim_iters = @time MILP(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
  							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
  							solver)
end

println("Solved. ARC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)

println("Writing instance...")

mkpath(output_name)

outDict = Dict()
outDict["Vals"] = val
outDict["Base"] = specs["Base"]
outDict["Paths"] = sol
outDict["Relation"] = assignDict["Relation"]
open(string(output_name, "/", output_name, ".yaml"), "w") do f
	YAMLwrite(f, outDict)
end

println("Writing plotable CSVs...")

function dict2plot(d)
    B = zeros(length(d)+1, 2)
    for i in Iterators.take(eachindex(B), length(d))
        B[i, :] = d[i]
    end
    B[length(d)+1, :] = d[1]
    return B
end

boundaries = specs["Boundaries"]
if haskey(specs, "Obstacles")
  obstacles = specs["Obstacles"]
else
	obstacles = Dict()
end
interest = buildDict["Interest"]
interest_nodes = buildDict["InterestNodes"]
pos = buildDict["Positions"]
base_pos = specs["Base"]["pos"]
base_angle = specs["Base"]["angle"]
base_inbound = buildDict["BaseInbound"]
base_outbound = buildDict["BaseOutbound"]
allpaths = buildDict["Paths"]
agents = specs["Agents"]
cover_radius = specs["CoverRadius"]
orientations = buildDict["Orientations"]

invrelation = Dict(v => k for (k, v) in assignDict["Relation"])

writecsv("$output_name/$output_name-region.csv", dict2plot(boundaries))
for (n, area) in obstacles
	B = dict2plot(area["Boundaries"])
  writecsv("$output_name/$output_name-obstacle$n.csv", B)
end
for (n, area) in interest
	B = dict2plot(area["Boundaries"])
  writecsv("$output_name/$output_name-interest$n.csv", B)
end
writecsv("$output_name/$output_name-nodes.csv", pos)
writecsv("$output_name/$output_name-interest-nodes.csv", pos[interest_nodes, :])
open(string(output_name, "/", output_name, "-interest-cover.tex"), "w") do f
  for node in interest_nodes
		println(f, "\\draw[dashed] (",pos[node,1],", ",pos[node,2],") circle, inner sep=0.01cm, minimum size=0.01cm (",cover_radius,");")
  end
end

colours = Dict(1 => "red", 2 => "blue")

println("Plotting...")

textwidth = 472
textheight = 652*0.98

B = dict2plot(boundaries)
plotregion = Axis(xlabel="X [space unit]", ylabel="Y [space unit]", width=textwidth, height=textheight, axisEqual=true)
push!(plotregion.plots, Plots.Linear(B[:, 1], B[:, 2], style="mark=none, color=black"))
for (n, area) in obstacles
	B = dict2plot(area["Boundaries"])
  push!(plotregion.plots, Plots.Linear(B[:, 1], B[:, 2], style="mark=none, color=black"))
end
for (n, area) in interest
	B = dict2plot(area["Boundaries"])
  push!(plotregion.plots, Plots.Linear(B[:, 1], B[:, 2], style="mark=none, dotted, color=black"))
end
push!(plotregion, Plots.Node("", base_pos[1], base_pos[2], style="draw, circle, inner sep=0.01cm, minimum size=0.01cm, blue"))
plotall = deepcopy(plotregion)
push!(plotall, Plots.Scatter(pos[: ,1], pos[: ,2], style="black"))
#scatter!(plotall, pos[:, 1], pos[:, 2], label="nodes")

save("visual-region-$output_name.svg", plotregion)
save("visual-segmentation-$output_name.svg", plotall)

push!(plotregion, Plots.Node(L"b^{R}", base_pos[1], base_pos[2], style="above left"))
push!(plotregion, Plots.Node("", base_pos[1], base_pos[2], style="circle, inner sep=0.02cm, minimum size=0.02cm, fill, draw, black"))
for i in interest_nodes
	push!(plotregion, Plots.Node("", pos[i, 1], pos[i, 2], style="circle, inner sep=0.02cm, minimum size=0.02cm, fill, draw, black"))
end
plotinterest = deepcopy(plotregion)
for i in interest_nodes
	C = zeros(15, 2)
	i = 0
	for t in linspace(0.0, 2*pi, 15)
		i += 1
  	C[i, 1] = pos[i, 1] + cover_radius*0.5*cos(t)
  	C[i, 2] = pos[i, 2] + cover_radius*0.5*sin(t)
	end
	push!(plotinterest, Plots.Linear(C[:,1], C[:,2], style="black, dotted"))
end

save("visual-interest-$output_name.svg", plotinterest)

#linestyles = Dict(1 => :dash, 2 => :dot)
#colours = Dict(1 => :red, 2 => :blue)

#plotpaths = deepcopy(plotinterest)
#if args["plot-all"]
  #for (startend, path) in allpaths
		#X = Float64[]
		#Y = Float64[]
  	#startState = startend[1]
  	#endState = startend[2]
  	#a = startend[3]
  	#for i in 1:length(path)-1
  		#s = path[i]
  		#e = path[i+1]
  		#p = buildBezier(s, e, agents[a]["minspeed"], pos, orientations)
			#for t in linspace(0.0, 1.0, 10)
				#push!(X, p.Px(t))
				#push!(Y, p.Py(t))
			#end
  		#push!(X, NaN)
  		#push!(Y, NaN)
  	#end
		#plot!(plotpaths, X, Y, linestyle=linestyles[a], color=colours[a], label="")
  #end
  #for a in 1:length(agents)
  	#p = buildBezier(base_pos, base_angle, base_inbound[a], agents[a]["minspeed"], pos, orientations)
  	#plot!(plotpaths, p.Px, p.Py, linspace(0.0, 1.0, 10), linestyle=linestyles[a], color=colours[a], label="")
  	#p = buildBezier(base_outbound[a], base_pos, mod(base_angle+pi, 2*pi), agents[a]["minspeed"], pos, orientations)
  	#plot!(plotpaths, p.Px, p.Py, linspace(0.0, 1.0, 10), linestyle=linestyles[a], color=colours[a], label="")
  #end
#end

plotsol = deepcopy(plotregion)
ntour = 0
colors = palette("Set1", 9)
lines = function(n)
	style = "dash pattern="
	if n > 1
		style = string(style, "on 5pt off 2pt ")
		for i in 1:n-2
			style = string(style, "on 1pt off 1pt ")
		end
		style = string(style, "on 1pt off 2pt")
	end
	return style
end
agents_name= Dict(1 => "UGV", 2 => "UAV")
for tour in sol
		ntour += 1
    a = tour.a
		ncolor = rem(ntour, length(colors))
		v_red = convert(Float64, red(colors[ncolor]))
		v_green = convert(Float64, green(colors[ncolor]))
		v_blue = convert(Float64, blue(colors[ncolor]))
		color = "{rgb:red,$v_red; green,$v_green; blue,$v_blue}"
		line = lines(ntour)
    for i in Iterators.take(eachindex(tour), length(tour)-1)
        s = State(invrelation[tour.states[i].node], tour.states[i].orientation)
        e = State(invrelation[tour.states[i+1].node], tour.states[i+1].orientation)
        b = first(allpaths[s.node, e.node, s.orientation, e.orientation, a])
        for f in Iterators.drop(allpaths[s.node, e.node, s.orientation, e.orientation, a], 1)
            p = buildBezier(b, f, agents[a]["minspeed"], pos, orientations)
						X = [p.Px(t) for t in linspace(0.0, 1.0, 10)]
						Y = [p.Py(t) for t in linspace(0.0, 1.0, 10)]
    				plt = Plots.Linear(X, Y, style="$line, ->, mark=none, color=$color")
    				push!(plotsol.plots, plt)
            b = f
        end
    end
		name = agents_name[a]
		insert!(plotsol.plots, 2*ntour-1, Plots.Command("\\addlegendimage{$line, color=$color}"))
		insert!(plotsol.plots, 2*ntour, Plots.Command("\\addlegendentry{Tour $ntour, Agent $name}"))
end
base_angle = base_angle/180*pi
for a in 1:length(agents)
    bi = buildDict["BaseInbound"][a]
    bo = buildDict["BaseOutbound"][a]
    p = buildBezier(base_pos, base_angle, bi, agents[a]["minspeed"], pos, orientations)
    push!(plotsol, Plots.Linear([p.Px(t) for t in linspace(0.0, 1.0, 10)], [p.Py(t) for t in linspace(0.0, 1.0, 10)], style=s"solid, mark=none, color=black"))
    p = buildBezier(bo, base_pos, mod(base_angle+pi, 2*pi), agents[a]["minspeed"], pos, orientations)
    push!(plotsol, Plots.Linear([p.Px(t) for t in linspace(0.0, 1.0, 10)], [p.Py(t) for t in linspace(0.0, 1.0, 10)], style=s"solid, mark=none, color=black"))
end

save("visual-$output_name.tex", plotsol, include_preamble=false)
#save("visual-$output_name.svg", plotsol)
if args["plot-all"]
  savefig(plotregion, "visual-region-$output_name.svg")
  savefig(plotall, "visual-grid-$output_name.svg")
  savefig(plotinterest, "visual-interest-$output_name.svg")
  savefig(plotpaths, "visual-paths-$output_name.svg")
end

println("Done.")
