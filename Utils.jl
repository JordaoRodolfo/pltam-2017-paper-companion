module Utils

export YAMLwrite

function YAMLwrite(f::IO, obj::String; level=1)
	prefix = repeat("  ", level-1)
	strplt = split(string("\"", obj, "\""), "\n")
	println(f, prefix, strplt[1])
	for i in 2:length(strplt)
		println(f, prefix, "  ", strplt[i])
	end
end

function YAMLwrite(f::IO, obj::Dict; level=1)
	prefix = repeat("  ", level-1)
	for (key, val) in obj
		println(f, prefix, key, ": ")
		YAMLwrite(f, val, level=level+1)
	end
end

function YAMLwrite(f::IO, obj::Array; level=1)
	prefix = repeat("  ", level-1)
	if length(size(obj)) > 1
		dims = [1:d for d in size(obj)]
		for i in dims[1]
			println(f, prefix, i, ": ")
			YAMLwrite(f, obj[i, dims[2:end]...], level=level+1)
		end
	elseif isa(obj, Array{Float64}) || isa(obj, Array{Int64})
		println(f, prefix, obj)
	else
		for i in eachindex(obj)
			println(f, prefix, i, ": ")
			YAMLwrite(f, obj[i], level=level+1)
		end
	end
end

function YAMLwrite(f, obj; level=1)
	prefix = repeat("  ", level-1)
	println(f, prefix, obj)
end

end
