module AssignStage

type State
	node::Int64
	orientation::Int64
end

Base.isequal(s1::State, s2::State) = s1.node == s2.node && s1.orientation == s2.orientation
Base.hash(s::State) = hash(s.node + s.orientation)
Base.copy(s::State) = State(s.node, s.orientation)
Base.in(s::State, sc::Array{State}) = any(isequal(s, si) for si in sc)
Base.isequal(t1::Tuple{State,State,Int64}, t2::Tuple{State,State,Int64}) = isequal(t1[1], t2[1]) && isequal(t1[2], t2[2]) && isequal(t1[3], t2[3])
Base.hash(t::Tuple{State,State,Int64}) = hash(hash(t[1]) + hash(t[2]) + hash(t[3]))

type Tour
	states::Array{States, 1}
	a::Int64
end

Base.copy(t::Tour) = Tour(copy(t.states), copy(t.a))
Base.deepcopy(t::Tour) = Tour(deepcopy(t.states), deepcopy(t.a))
Base.start(t::Tour) = start(t.states)
Base.next(t::Tour, state) = next(t.states, state)
Base.done(t::Tour, state) = done(t.states, state)
Base.length(t::Tour) = length(t.states)
Base.getindex(t::Tour, index) = getindex(t.states, index)
Base.setindex!(t::Tour, X, index) = setindex!(t.states, X, index)
Base.view(t::Tour, index) = view(t.states, index)
Base.endof(t::Tour) = length(t.states)
function Base.isequal(t1::Tour, t2::Tour)
	if t1.a != t2.a || length(t1) != length(t2)
		return false
	end
	for i in t1
		if !(i in t2)
			return false
		end
	end
	return true
end

function hasintersection(t1::Tour, t2::Tour)
	if t1.a == t2.a
		for i in t1[2:end-1]
			if i in t2[2:end-1]
				return true
			end
		end
	end
	return false
end

function equalTourArrays(Ri, Rj)
	for ri in Ri
		equals = false
		for rj in Rj
			equals = equals || isequal(ri, rj)
		end
		if !equals
			return false
		end
	end
	return true
end

function tourInsideSet(q, Q)
	inside = false
	for qj in Q
		inside = inside || equalTourArrays(q, qj)
	end
	return inside
end

modn(x, n) = x > n ? x - n : x

function pushsorted!(v, x)
	i = 1
	n = size(v, 1)
	while i <= n && v[i] <= x
		i += 1
	end
	insert!(v, i, x)
end

function tourCost(tour::Tour, NodeCosts, Costs)
	# must start and end in the base_n
	cost = 0.0
	n = size(tour.states, 1)
	nops = size(NodeCosts, 2)
	# println(DirectCosts)
	cost += DirectCosts[tour.nodes[1], tour.nodes[2], tour.a]
	for i = 2:n-1
		cost += sum(NodeCosts[tour.nodes[i], :])
		cost += DirectCosts[tour.nodes[i], tour.nodes[i+1], tour.a]
		cost += TurnCosts[tour.nodes[i-1], tour.nodes[i], tour.nodes[i+1], tour.a]
		# start = Paths[(cur, start)][2]
		# finish = Paths[(cur, finish)][2]
	end
  return cost
end

function tourCost(tour, a, NodeCosts, DirectCosts, TurnCosts)
	# must start and end in the base_n
	cost = 0.0
	# n = size(tour.nodes, 1)
	nops = size(NodeCosts, 2)
	# println(DirectCosts)
	simm = start(tour)
	imm, sim = next(tour, simm)
	im, si = next(tour, sim)
	# cost += sum(NodeCosts[im, :])
	cost += DirectCosts[imm, im, a]
	while !done(tour, si)
		imm, simm = next(tour, simm)
		im, sim = next(tour, sim)
		i, si = next(tour, si)
		cost += sum(NodeCosts[im, :])
		cost += DirectCosts[im, i, a]
		cost += TurnCosts[imm, im, i, a]
	end
  return cost
end

function tourDifference(Rs, DirectCosts, TurnCosts, Paths)
	n = size(Rs, 1)
	d = 0
	if n > 2
		for i=1:n-1, j=i+1:n
			addsquare = tourCost(Rs[i], DirectCosts, TurnCosts)
			addsquare -= tourCost(Rs[j], DirectCosts, TurnCosts)
			d += addsquare*addsquare
		end
	end
	return d
end

function H(R::Array{Tour}, NodeCosts, DirectCosts, TurnCosts)
	# h += tourCost(R, NodeCosts, DirectCosts, TurnCosts)
	return sum(map(r -> tourCost(r, NodeCosts, DirectCosts, TurnCosts), R))
end

function removalCost(R::Tour, i, NodeCosts, DirectCosts, TurnCosts)
	n = length(R)
	a = R.a
	cn = NodeCosts
	cd = DirectCosts
	ct = TurnCosts
	cost = -cd[R[i-1], R[i], a] - cd[R[i], R[i+1], a] - ct[R[i-1], R[i], R[i+1], a] +
					cd[R[i-1], R[i+1], a] - sum(cn[R[i], :])
	if i > 2
		cost += ct[R[i-2], R[i-1], R[i+1], a] - ct[R[i-2], R[i-1], R[i], a]
	end
	if i < n-1
		cost += ct[R[i-1], R[i+1], R[i+2], a] - ct[R[i], R[i+1], R[i+2], a]
	end
	return cost
end

function insertionCost(R::Tour, i, pos, NodeCosts, DirectCosts, TurnCosts)
	n = length(R)
	a = R.a
	cn = NodeCosts
	cd = DirectCosts
	ct = TurnCosts
	cost = cd[R[pos-1], i, a] + cd[i, R[pos], a] - cd[R[pos-1], R[pos], a] -
				 ct[R[pos-1], R[pos], R[pos+1], a] + ct[R[pos-1], i, R[pos], a] + ct[i, R[pos], R[pos+1], a] +
				 sum(cn[i, :])
	if pos > 2
		cost += ct[R[pos-2], R[pos-1], i, a] - ct[R[pos-2], R[pos-1], R[pos], a]
	end
	# if pos < length(R)-1
	# 	cost += ct[i, R[pos+1], R[pos+2], a] - ct[R[pos], R[pos+1], R[pos+2], a]
	# end
	return cost
end

function mergeCost(tour1::Tour, tour2::Tour, NodeCosts, DirectCosts, TurnCosts)
	var12 = DirectCosts[tour1[end-1], tour2[2]] - DirectCosts[tour1[end-1], tour1[end]] - DirectCosts[tour2[1], tour2[2]] -
		TurnCosts[tour1[end-2], tour1[end-1], tour1[end]] - TurnCosts[tour2[1], tour2[2], tour2[3]] +
		TurnCosts[tour1[end-2], tour1[end-1], tour2[2]] + TurnCosts[tour1[end-1], tour2[2], tour2[3]]
	var21 = DirectCosts[tour2[end-1], tour1[2]] - DirectCosts[tour2[end-1], tour2[end]] - DirectCosts[tour1[1], tour1[2]] -
		TurnCosts[tour2[end-2], tour2[end-1], tour2[end]] - TurnCosts[tour1[1], tour1[2], tour1[3]] +
		TurnCosts[tour2[end-2], tour2[end-1], tour1[2]] + TurnCosts[tour2[end-1], tour1[2], tour1[3]]
	if var12 >= var21
		return var12, 1
	else
		return var21, -1
  end
end

function updateIncidence(tour::Tour, incidence)
	# println(tour)
	for (place, val) in enumerate(tour[2:end-1])
		# +1 is beacuase enumarate starts from 1 and not 2
		incidence[place+1][val] += 1
	end
	#println(incidence)
	return incidence
end

function leastIncident(base_n, len, a, incidence)
	tour = Tour(fill(0, len), a)
	tour[1] = base_n
	tour[len] = base_n
	for (place, v) in incidence
		possibles = filter((val, count) -> !(val in tour), v)
		toadd = reduce((val1, val2) -> val1[2] > val2[2] ? val2 : val1, possibles)
		tour[place] = toadd[1]
	end
	return tour
end

function tourOptimization(tour::Tour, NodeCosts, DirectCosts, TurnCosts; minPercent=0.0001, maxIters=100)
	n = length(tour)
	bestcost = tourCost(tour, tour.a, NodeCosts, DirectCosts, TurnCosts)
	besttour = copy(tour)
	a = tour.a
	iters = 1
	percent = 1.0
	visited = Tour[]
	incidence = Dict(i => Dict(j => 0 for j in tour[2:n-1]) for i in 2:n-1)
	if n <= 3
		return tour
	end
	while percent > minPercent && iters < maxIters
		incidence = updateIncidence(tour, incidence)
		mark = 0, 0
		curcost = tourCost(tour, tour.a, NodeCosts, DirectCosts, TurnCosts)
		for i = 2:n-2, j = i+1:n-1
			iter = Base.Iterators.flatten((tour[1:i-1], tour[j], tour[i+1:j-1], tour[i], tour[j+1:n]))
			nextcost = tourCost(iter, tour.a, NodeCosts, DirectCosts, TurnCosts)
			if nextcost < curcost
				curcost = nextcost
				mark = i, j
			end
		end
		if !(tour in visited)
			push!(visited, copy(tour))
		end
		if mark[1] != 0 || mark[2] != 0
			i = tour[mark[1]]
			tour[mark[1]] = tour[mark[2]]
			tour[mark[2]] = i
		else
			if curcost < bestcost
				percent = bestcost/curcost - 1.0
				bestcost = curcost
				besttour = copy(tour)
			else
				percent = curcost/bestcost - 1.0
			end
			# tour.nodes[2:n-1] = shuffle(tour.nodes[2:n-1])
			# while tour in visited
			# 	tour.nodes[2:n-1] = shuffle(tour.nodes[2:n-1])
			# end
			tour_least = leastIncident(tour[1], length(tour), a, incidence)
			if isequal(tour.nodes, tour_least.nodes)
				return besttour
			else
				tour = tour_least
			end
		end
		iters += 1
	end
	tour = besttour
	return tour
end

function tourTransfer(R, ir, jr, i, j, dir)
	if dir == 0
		return R
	end
	Rij = deepcopy(R)
	if dir == 1
		insert!(Rij[jr].nodes, j, Rij[ir].nodes[i])
		deleteat!(Rij[ir].nodes, i)
		if length(Rij[ir]) <= 2
			deleteat!(Rij, ir)
		end
	elseif dir == -1
		insert!(Rij[ir].nodes, i, Rij[jr].nodes[j])
		deleteat!(Rij[jr].nodes, j)
		if length(Rij[jr]) <= 2
			deleteat!(Rij, jr)
		end
	end
	return Rij
end

function tourMerge(R, i, j, dir)
	Tij = Tour([], R[i].a)
	if dir == 1
		Tij.nodes = [R[i].nodes[1:end-1]; R[j].nodes[2:end]]
	else
		Tij.nodes = [R[j].nodes[1:end-1]; R[i].nodes[2:end]]
  end
	Rij = deepcopy(R)
	push!(Rij, Tij)
	deleteat!(Rij, i)
	deleteat!(Rij, j-1)
  return Rij
end

function BestTransferCost(R, ir, jr, NodeCosts, DirectCosts, TurnCosts)
	cd = DirectCosts
	ct = TurnCosts
	Ri, Rj = R[ir], R[jr]
	ni = length(Ri)
	nj = length(Rj)
	a = Ri.a
	mark = 0, 0, 0
	variation = 0.0
	for i=2:ni-1, j=2:nj-1
  	# Ri -> Rj
		varij = removalCost(Ri, i, NodeCosts, DirectCosts, TurnCosts) + insertionCost(Rj, Ri[i], j, NodeCosts, DirectCosts, TurnCosts)
		if varij < variation
			variation = varij
			mark = i, j, 1
		end
  	# Rj -> Ri
		varji = insertionCost(Ri, Rj[j], i, NodeCosts, DirectCosts, TurnCosts) + removalCost(Rj, j, NodeCosts, DirectCosts, TurnCosts)
		if varji < variation
			variation = varji
			mark = i, j, -1
		end
  end
	return variation, mark
end

function treeLevel(node, parents)
	if haskey(parents, node)
		return 1 + treeLevel(parents[node], parents)
	else
		return 1
	end
end

function updateScore(node, parents, score::Dict{Tour, Float64})
	curnode = node
	while haskey(parents, curnode)
		for r in curnode
			if !haskey(score, curnode)
				score[r] = 0.0
			end
			score[r] += 1.0
		end
		curnode = parents[curnode]
	end
end

function updateScore(node, parents, score::Dict{Tour, Int64})
	curnode = node
	while haskey(parents, curnode)
		for r in curnode
			if !haskey(score, curnode)
				score[r] = 0
			end
			score[r] += 1
		end
		curnode = parents[curnode]
	end
end

function leastVisited(constraints, frequencies)
	R = Tour[]
	possibles = copy(frequencies)
	while !constraints(R)
		# possibles = filter(kv -> !(kv[1] in R), frequencies)
		least = reduce((f1, f2) -> frequencies[f1] >= frequencies[f2] ? f2 : f1, keys(possibles))
		if all(r -> !hasintersection(r, least), R)
			push!(R, least)
		end
		delete!(possibles, least)
	end
	return R
end

function ARC(base_n, NodeCosts, DirectCosts, TurnCosts, constraints; MinPercent=0.05, tourMaxIters=100, maxIters=100)
	N = size(DirectCosts, 1)
	num_agents = size(DirectCosts, 3)
	parents = Dict{Array{Tour, 1}, Array{Tour, 1}}()
	percent = 1.0
	iters = 1
	# visited = Array{Array{Tour, 1}, 1}()
	R = [Tour([base_n, i, base_n], a) for i in 1:N, a in 1:num_agents if i != base_n]
	vals = Dict(R => H(R, NodeCosts, DirectCosts, TurnCosts))
	frequency = Dict{Tour, Int64}()
	bestSol = R
	Hval(R) = H(R, NodeCosts, DirectCosts, TurnCosts)
	while percent > MinPercent && iters < maxIters
		if rem(iters, 200) == 0
			println("Iter: ", iters, " percent: ", percent)
		end
		for r in R
			tourOptimization(r, NodeCosts, DirectCosts, TurnCosts, maxIters=tourMaxIters)
		end
		best_var = 0.0
		best_R = R
		for i in 1:length(R) # goes to length beacuse of removal
			for j in i+1:length(R)
				if i != j && R[i].a == R[j].a
					variation, mark = BestTransferCost(R, i, j, NodeCosts, DirectCosts, TurnCosts)
					if variation < best_var
						Rij = tourTransfer(R, i, j, mark...)
						if constraints(Rij)# && !tourInsideSet(Rij, visited)
							vals[Rij] = Hval(Rij)
							parents[Rij] = R
							best_var = variation
							best_R = Rij
						end
					end
					variation, dir = mergeCost(R[i], R[j], NodeCosts, DirectCosts, TurnCosts)
					if variation < best_var
            Rij = tourMerge(R, i, j, dir)
						if constraints(Rij)# && !tourInsideSet(Rij, visited)
							vals[Rij] = Hval(Rij)
							parents[Rij] = R
							best_var = variation
							best_R = Rij
						end
          end
				end
			end
			variation = -tourCost(R[i], NodeCosts, DirectCosts, TurnCosts)
			if variation < best_var
				Ri = deleteat!(deepcopy(R), i)
				valRi = Hval(Ri)
				if constraints(Ri)# && !tourInsideSet(Ri, visited)
					vals[Ri] = valRi
					parents[Ri] = R
					best_var = variation
					best_R = Ri
				end
			end
		end
		# if !tourInsideSet(R, visited)
		# 	push!(visited, R)
		# end
		if !equalTourArrays(R, best_R)
			R = best_R
		else
			if vals[R] < vals[bestSol]
				percent = vals[bestSol]/vals[R] - 1.0
				bestSol = R
			else
				percent = vals[R]/vals[bestSol] - 1.0
			end
			updateScore(R, parents, frequency)
			R_least = leastVisited(constraints, frequency)
			if equalTourArrays(R, R_least)
				return bestSol, vals[bestSol]
			else
				R = R_least
				vals[R] = Hval(R)
			end
		end
		iters += 1
	end
	return bestSol, vals[bestSol]
end


function constraintBeta(tours::Array{Tour}, NodeCosts, DirectCosts, TurnCosts, beta_val)
	feasible = true
	for tour in tours
		if tourCost(tour, NodeCosts, DirectCosts, TurnCosts) > beta_val
			feasible = false
		end
	end
	return feasible
end

function constraintDemand(tours::Array{Tour}, demand, supply)
	num_operations = size(demand, 2)
	num_nodes = size(demand, 1)
	supplied = fill(false, (num_nodes, num_operations))
	for op in 1:num_operations
		for r in tours
			for i in r
				supplied[i, op] = supplied[i, op] || supply[r.a, op] #+= supply[r.a, op]
			end
		end
	end
	return all(supplied .| .!demand)#any(.!(supplied .> demand))
end

using Plots
using ArgParse
using YAML
include("Utils.jl")
using Utils

s = ArgParseSettings()

@add_arg_table s begin
    "--dim", "-d"
        help = "Size of the grid"
				arg_type = Int
				default = 4
    "--vehicles", "-v"
        help = "Number of vehicles"
        arg_type = Int
        default = 2
    "--time", "-t"
        help = "Time of the temporal formulation"
        arg_type = Int
        default = 10
    "--maxtime"
        help = "Maximum time made by the solver"
        arg_type = Float64
        default = 10000.0
    "--beta", "-b"
        help = "Maximum size of each vehicle tour"
        arg_type = Int
        default = 30
		"--tolerance"
        help = "relative percentage of solution accepted"
        arg_type = Float64
        default = 0.05
		"--num-sols"
        help = "Maximum number fo solutions"
        arg_type = Int
        default = 1
		"inputData"
        help = "The instance declaration file. Only YAML supported now."
        arg_type = String
        default = "instance-assign.yaml"
end

args = parse_args(s)

println("Chosen parameters: ")
println("Instance: ", args["inputData"])
# println("grid dimension: ", args["dim"])
# println("vehicles: ", args["vehicles"])
println("time: ", args["time"])
println("Max time: ", args["maxtime"])
println("beta: ", args["beta"])
println("tolerance: ", args["tolerance"])
println("num-sols: ", args["num-sols"])

println("Starting printing engine...")

# gr()
pyplot()

println("Reading instance...")

instance = YAML.load(open(args["inputData"]))

println("Setting parameters...")

base_n = instance["Base"]
N = instance["Nodes"]
agents = instance["Agents"]
num_agents = length(agents)
num_operations = instance["Operations"]
interest = instance["Interest"]
boundaries = instance["Boundaries"]
obstacles = instance["Obstacles"]
beta_val = args["beta"]
sol_tolerance = args["tolerance"]
num_sols = args["num-sols"]

println("Building model...")

relation = Dict{Int64,Int64}()
invrelation = Dict{Int64,Int64}()
NodeCosts = zeros(N, num_operations)
i = 1
for (n, c) in instance["NodeCosts"]
	relation[n] = i
	invrelation[i] = n
	NodeCosts[i, :] = c
	i += 1
end
demand = NodeCosts .> 0.0

supply = zeros(num_agents, num_operations)
for (na, va) in agents
	for (no, vo) in va["operation"]
		supply[na, no] = 1
	end
end

Positions = zeros(base_n, 2)
for (node, pos) in instance["Positions"]
	Positions[node, :] = pos
end
# println(instance["Positions"])
# Neighbors = Dict(1 => [2])
# for (node, neigh) in instance["Neighbors"]
# 	Neighbors[node] = neigh
# end
TurnCosts = zeros(N, N, N, num_agents)
for (i, v1) in instance["TurnCosts"]
	for (j, v2) in v1
		for (k, v3) in v2
			for (a, c) in enumerate(v3)
				TurnCosts[relation[i], relation[j], relation[k], a] = c
			end
		end
	end
end

DirectCosts = zeros(N, N, num_agents)
for (i, v1) in instance["DirectCosts"]
	for (j, v2) in v1
		for (a, c) in enumerate(v2)
			DirectCosts[relation[i], relation[j], a] = c
		end
	end
end

Paths = Dict((1,2,1) => [1; 2])
for (i, v1) in instance["Paths"]
	for (j, v2) in v1
		for (a, p) in v2
			Paths[(i,j,a)] = p
		end
	end
end

println("Solving...")

constraintsFun(R) = constraintBeta(R, NodeCosts, DirectCosts, TurnCosts, beta_val) &&
	constraintDemand(R, demand, supply)

sol, val = ARC(relation[base_n], NodeCosts, DirectCosts, TurnCosts, constraintsFun, MinPercent=1e-6, tourMaxIters=1000, maxIters=3000)
# ARC(base_n, N, num_agents, Hval, MinPercent=0.05, SAtries=10)

println("Solved.")

println("Printing solution...")
instance_name, _ = split(basename(args["inputData"]), ".")
file_name, _ = split(basename(@__FILE__), ".")

outDict = Dict()
outDict["ObjFun"] = val
outDict["Base"] = relation[base_n]
outDict["Paths"] = Dict(i => Dict("Nodes" => sol[i].nodes, "Agent" => sol[i].a) for i in 1:length(sol))
open("res-$file_name-$instance_name.yaml", "w") do f
	YAMLwrite(f, outDict)
end

#println(clusters)
println("Plotting solution...")

bounds = zeros(length(boundaries), 2)
for (nb, vb) in boundaries
	bounds[nb, :] = vb
end
bounds = [bounds; bounds[1, 1] bounds[1, 2]]
plot(bounds[:,1], bounds[:,2], label="Boundary")

for (ni, vi) in obstacles
	obsi = zeros(length(vi["Boundaries"])+1, 2)
	for (nb, vb) in vi["Boundaries"]
		obsi[nb, :] = vb
	end
	obsi[end, :] = obsi[1, :]
	plot!(obsi[:,1], obsi[:,2], label="Obstacles $ni")
end

inter = zeros(0, 2)
for (ni, vi) in interest
	interi = zeros(length(vi["Boundaries"])+1, 2)
	for (nb, vb) in vi["Boundaries"]
		interi[nb, :] = vb
	end
	interi[end, :] = interi[1, :]
	plot!(interi[:,1], interi[:,2], label="Interest $ni")
end

nodes_interest = collect(keys(relation))
scatter!(Positions[nodes_interest, 1], Positions[nodes_interest, 2], label="Nodes")

base_n_x, base_n_y = Positions[base_n, :]
scatter!([base_n_x], [base_n_y], label="Base")

for a in 1:num_agents
	tours_a = filter(t -> t.a == a, sol)
	route = zeros(0, 2)
	for r in tours_a
		for i in 1:size(r.nodes, 1)-1
			for p in Paths[(invrelation[r.nodes[i]], invrelation[r.nodes[i+1]], a)]
				# println(p)
				route = [route; Positions[p, 1] Positions[p, 2]]
			end
		end
		route = [route; NaN NaN]
		# println(route)
	end
	plot!(route[:, 1], route[:, 2], label="Agent $a")
end

savefig("visual-res-$file_name-$instance_name.png")

# open("res-$file_name-$instance_name.yaml", "w") do f
# 	write(f, "Paths:\n")
# 	for R in Rs
# 		write(f, "  -- $R\n")
# 	end
# end

# plot(J, label="Criteria")
#
# savefig("results/visual-res-objfun-$file_name-$instance_name.png")
#
# writecsv("results/res-objfun-$file_name-$instance_name.csv", J)

println("Done.")

# DEPRECATED

function ARCQ(base_n, NodeCosts, DirectCosts, TurnCosts, constraints; MinPercent=0.05, maxIters=1000)
	N = size(DirectCosts, 1)
	num_agents = size(DirectCosts, 3)
	parents = Dict{Array{Tour, 1}, Array{Tour, 1}}()
	percent = 1.0
	iters = 1
	Q = Array{Array{Tour, 1}, 1}()
	push!(Q, [Tour([base_n, i, base_n], a) for i in 1:N, a in 1:num_agents if i != base_n])
	vals = Dict(Q[1] => H(Q[1], NodeCosts, DirectCosts, TurnCosts))
	# score = Dict{Tour, Int64}()
	# solutions = Array{Array{Tour, 1}, 1}()
	bestSol = Q[1]
	# visited = [R]
	Hval(R) = H(R, NodeCosts, DirectCosts, TurnCosts)
	# scoreVal(R) = sum(map(r -> haskey(score, r) ? score[r] : 1.0, R))
	while percent > MinPercent && iters < maxIters && length(Q) > 0
		if iters % 100 == 0
			println("iters: ", iters, ", Q: ", length(Q))
		end
		Rind = first(indmin(map(Ri -> vals[Ri], Q)))
		R = Q[Rind]
		deleteat!(Q, Rind)
		for r in R
			tourOptimization(r, DirectCosts, TurnCosts, SAtries)
		end
		best_var = 0.0
		# best_R = R
		for i in eachindex(R)
			for j in eachindex(R)
				# println(i, ", ", j)
				if i != j && R[i].a == R[j].a
					Rij, variation = BestTransfer(R, i, j, DirectCosts, TurnCosts)
					if variation < 0.0 && constraints(Rij) && !tourInsideSet(Rij, Q)
						push!(Q, Rij)
						vals[Rij] = Hval(Rij)
						parents[Rij] = R
						best_var = variation
					end
					# scoreRij = valRij*sum(map(r -> haskey(score, r) ? score[r] : 1.0, Rij))
					# if scoreRij < best_score && constraints(Rij)
					# 	vals[Rij] = valRij
					# 	best_score = scoreRij
					# 	best_R = Rij
					# 	# println("exchange: ", i, " and ", j)
					# end
				end
			end
			Ri = deleteat!(deepcopy(R), i)
			# scoreRi = valRi*sum(map(r -> haskey(score, r) ? score[r] : 1.0, Ri))
			if constraints(Ri) && !tourInsideSet(Ri, Q)
				push!(Q, Ri)
				valRi = Hval(Ri)
				vals[Ri] = valRi
				parents[Ri] = R
				best_var = vals[R] - vals[Ri]
				# best_score = scoreRi
				# best_R = Ri
				# println("delete: ", i)
			end
		end
		if best_var < 0.0
			# push!(solutions, best_R)
			# forEachBranch(function(node)
			if vals[R] < vals[bestSol]
				percent = (1 - vals[R]/vals[bestSol])
				bestSol = R
				# println("Best sol at ", vals[bestSol])
				println("percent ", percent)
				println("iters ", iters)
			end
			# updateScore(R, parents, score)
			# println("min found at ", iters, ", Q size: ", length(Q))
			# R = rand(visited)
		end
		# if !(R in visited)
		# 	push!(visited, R)
		# end
		iters += 1
		# println(iters)
	end
	# bestSol = reduce((s1, s2) -> vals[s1] > vals[s2] ? s2 : s1, Q)
	# println(score)
	println(percent)
	println(iters)
	return bestSol, vals[bestSol]
end

function BestTransfer(R, ir, jr, NodeCosts, DirectCosts, TurnCosts)
	cd = DirectCosts
	ct = TurnCosts
	Ri, Rj = R[ir], R[jr]
	ni = length(Ri)
	nj = length(Rj)
	a = Ri.a
	mark = 0, 0, 0
	variation = 0.0
	# println(Ri, ", ", ni)
	# println(Rj, ", ", nj)
	for i=2:ni-1, j=2:nj-1
  	# Ri -> Rj
		# println(i, j)
		vari = removalCost(Ri, i, NodeCosts, DirectCosts, TurnCosts)
		# vari = -cd[Ri[i-1], Ri[i], a] - cd[Ri[i], Ri[i+1], a] - ct[Ri[i-1], Ri[i], Ri[i+1], a] +
		# 				cd[Ri[i-1], Ri[i+1], a]
		# if i > 2
		# 	vari += ct[Ri[i-2], Ri[i-1], Ri[i+1], a] - ct[Ri[i-2], Ri[i-1], Ri[i], a]
		# end
		# if i < ni-1
		# 	vari += ct[Ri[i-1], Ri[i+1], Ri[i+2], a] - ct[Ri[i], Ri[i+1], Ri[i+2], a]
		# end
		varj = insertionCost(Rj, Ri[i], j, NodeCosts, DirectCosts, TurnCosts)
		# varj = cd[Rj[j-1], Ri[i], a] + cd[Ri[i], Rj[j], a] - cd[Rj[j-1], Rj[j], a] -
		# 			 ct[Rj[j-1], Rj[j], Rj[j+1], a] + ct[Rj[j-1], Ri[i], Rj[j], a] + ct[Ri[i], Rj[j], Rj[j+1], a]
		# if j > 2
		# 	varj += ct[Rj[j-2], Rj[j-1], Ri[i], a] - ct[Rj[j-2], Rj[j-1], Rj[j], a]
		# end
		# if j < nj-1
		# 	varj += ct[Ri[i], Rj[j+1], Rj[j+2], a] - ct[Rj[j], Rj[j+1], Rj[j+2], a]
		# end
		if vari + varj < variation
			variation = vari + varj
			mark = i, j, 1
		end
  	# Rj -> Ri
		varj = removalCost(Rj, j, NodeCosts, DirectCosts, TurnCosts)
		# varj = -cd[Rj[j-1], Rj[j], a] - cd[Rj[j], Rj[j+1], a] - ct[Rj[j-1], Rj[j], Rj[j+1], a] +
		# 				cd[Rj[j-1], Rj[j+1], a]
		# if j > 2
		# 	varj += ct[Rj[j-2], Rj[j-1], Rj[j+1], a] - ct[Rj[j-2], Rj[j-1], Rj[j], a]
		# end
		# if j < nj-2
		# 	varj += ct[Rj[j-1], Rj[j+1], Rj[j+2], a] - ct[Rj[j], Rj[j+1], Rj[j+2], a]
		# end
		vari = insertionCost(Ri, Rj[j], i, NodeCosts, DirectCosts, TurnCosts)
		# vaRi = cd[Ri[i-1], Rj[j], a] + cd[Rj[j], Ri[i], a] - cd[Ri[i-1], Ri[i], a] -
		# 			 ct[Ri[i-1], Ri[i], Ri[i+1], a] + ct[Ri[i-1], Rj[j], Ri[i], a] + ct[Rj[j], Ri[i], Ri[i+1], a]
		# if i > 2
		# 	vaRi += ct[Ri[i-2], Ri[i-1], Rj[j], a] - ct[Ri[i-2], Ri[i-1], Ri[i], a]
		# end
		# if i < ni-2
		# 	vaRi += ct[Rj[j], Ri[i+1], Ri[i+2], a] - ct[Ri[i], Ri[i+1], Ri[i+2], a]
		# end
		if vari + varj < variation
			variation = vari + varj
			mark = j, i, -1
		end
  end
	# println(R)
	if mark[3] != 0
		Rij = deepcopy(R)
		# println(Rij[ir], ", ", Rij[jr])
		if mark[3] == 1
			i, j = mark[1:2]
			# swapBetweenTours(Rij[ir].nodes, Rij[jr].nodes, i, j)
			insert!(Rij[jr].nodes, j, Rij[ir].nodes[i])
			deleteat!(Rij[ir].nodes, i)
			if length(Rij[ir]) <= 2
				deleteat!(Rij, ir)
			end
			# println(Rij[ir], ", ", length(Rij[ir]))
			# bestRi = copy(Ri)
			# bestRj = copy(Rj)
			# swapBetweenTours(Rj.nodes, Ri.nodes, j, i)
		elseif mark[3] == -1
			j, i = mark[1:2]
			# swapBetweenTours(Rij[jr].nodes, Rij[ir].nodes, j, i)
			insert!(Rij[ir].nodes, i, Rij[jr].nodes[j])
			deleteat!(Rij[jr].nodes, j)
			if length(Rij[jr]) <= 2
				deleteat!(Rij, jr)
			end
			# println(Rij[ir], ", ", length(Rij[ir]))
			# bestRi = copy(Ri)
			# bestRj = copy(Rj)
			# swapBetweenTours(Ri.nodes, Rj.nodes, i, j)
		end
		return Rij, variation
	end
	# println(R)
	return R, variation
end

function tourOptimizationSA(tour::Tour, DirectCosts, TurnCosts, tries=20)
	n = size(tour.nodes, 1)
	bestvar = 0.0
	besttour = copy(tour.nodes)
	cd = DirectCosts
	ct = TurnCosts
	a = tour.a
	mark = 0, 0
	if n > 3
  	for t in 1:tries
			curvar = 1.0
			nextvar = 0.0
			found = false
			while !found
				mark = 0, 0
  			for i = 2:n-2, j = i+1:n-1
  				# swap(tour.nodes, i, j)
					# curvar = tourCost(tour, DirectCosts, TurnCosts)
					ni = view(tour.nodes, i-1:i+1)
					nj = view(tour.nodes, j-1:j+1)
					nextvar = cd[ni[1], ni[2], a] - cd[ni[2], ni[3], a] - ct[ni[1], ni[2], ni[3], a] +
		            cd[ni[1], nj[2], a] + cd[nj[2], ni[3], a] + ct[ni[1], nj[2], ni[3], a] -
		            cd[nj[1], nj[2], a] - cd[nj[2], nj[3], a] - ct[nj[1], nj[2], nj[3], a] +
		            cd[nj[1], ni[2], a] + cd[ni[2], nj[3], a] + ct[nj[1], ni[2], nj[3], a]
					if nextvar < curvar
						curvar = nextvar
						mark = i, j
					end
  			end
				if sum(mark) > 0
					swap(tour.nodes, mark...)
				else
					found = true
				end
			end
			if curvar < bestvar
				bestvar  = curvar
				besttour = copy(tour.nodes)
			end
			# for i in eachindex(1:rand(1:n))
			swap(tour.nodes, rand(2:n-1), rand(2:n-1))
			# end
			# shuffle!(tour.nodes[2:n-1])
  	end
	end
	tour.nodes = besttour
	#return bestH, besttour
end

function swap(C, i, j)
		temp = C[i]
		C[i] = C[j]
		C[j] = temp
end

function swapBetweenTours(Cfrom, Cto, from, to)
	insert!(Cto, to, Cfrom[from])
	deleteat!(Cfrom, from)
end
