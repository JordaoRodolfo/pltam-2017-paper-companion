module Algs

export bestPaths

function bestPaths(base, Neighbors, DirectCosts, TurnCosts)
	N = size(DirectCosts, 1)
	values = Dict(i => Inf for i in 1:N)
	values[base] = 0
	paths = Dict(base => [base])
	visited = [base]
	open = Int64[]
	parents = Dict(base => base)
	# immediate Neighbors
	for neigh in Neighbors[base]
		values[neigh] = DirectCosts[base, neigh]
		parents[neigh] = base
		push!(open, neigh)
	end
	# expand to distant Neighbors
	while size(open, 1) > 0
		node = first(open)
		for u in Neighbors[node]
			if values[node] + DirectCosts[node, u] + TurnCosts[parents[node], node, u] < values[u]
				values[u] = values[node] + DirectCosts[node, u] + TurnCosts[parents[node], node, u]
    		parents[u] = node
			end
			if !(u in visited) && !(u in open)
				push!(open, u)
			end
		end
		push!(visited, node)
		deleteat!(open, 1)
	end
	# recover paths
	for i in 1:base-1
		node = i
		paths[i] = [node]
		while !(base in paths[i])
			node = parents[node]
			insert!(paths[i], 1, node)
		end
	end
	for i in base+1:N
		node = i
		paths[node] = [node]
		while !(base in paths[i])
			node = parents[node]
			insert!(paths[i], 1, node)
		end
	end
	return values, paths
end

function bestPathsA(base, goals, Neighbors, Positions, DirectCosts, TurnCosts)
	N = size(DirectCosts, 1)
	values = Dict(i => Inf for i in 1:N)
	paths = Dict(base => [base])
	values[base] = 0
	visited = [base]
	parents = Dict(base => base)
	h(node, finish) = norm(Positions[finish, :] - Positions[node, :], 2)
	for neigh in Neighbors[base]
		values[neigh] = DirectCosts[base, neigh]
		parents[neigh] = base
		push!(visited, neigh)
		paths[neigh] = [base, neigh]
	end
	for node in visited
  	for u in Neighbors[node]
  		if !(u in visited) && !(u in open)
  			push!(open, u)
  		end
  	end
	end
	while !map(x -> x in visited, goals)
  		best = reduce((x, y) -> stack[x] > stack[y] ? y : x, keys(stack))
 			for neigh in Neighbors[best]
				if !(neigh in keys(stack) || neigh in keys(visited))
  				val = h(neigh) + stack[best] - h(best) + DirectCosts[best, neigh] + TurnCosts[parents[best], best, neigh]
					stack[neigh] = val
  				parents[neigh] = best
        end
			end
			visited[best] = stack[best]
			delete!(stack, best)
			if finish in keys(visited)
				found = true
			end
	end
		stack = Dict(i => h(i) + DirectCosts[start, i] for i in Neighbors[start])
		parents = Dict(i => start for i in Neighbors[start])
  	visited = Dict(start => h(start))
		found = false
  	while !found
  	end
		#println(visited)
		#println(stack)
  	path = [finish]
		while !(start in path)
			actual = parents[path[1]]
			insert!(path, 1, actual)
		end
		return values, paths
end

end
