# PLTAM ARC Compation repository

## Description

This repository houses the julia code and scripts to reproduce all the data used in
article "Cooperative Planning Methodology for Persistent Long Term Autonomous Missions",
by the author and others.

The main script to be learned and used is build-and-assign.jl, which gets a YAML problem
description and computes all stages for it: segmentation, path making and assingment. The
scripts runall.sh and extract-time.py are here simply for illustration of the process
used for running many instances and gathering enough statistical data for research.

Note that there are 7 situations listed here, against just two on the paper: 5 and 6
are the most expressive ones and were chosen to represent all the ideas dispersed into
these seven instances, altough you are free to mix and match with them however you like.

## Requirements

Obviously, Julia is a requirement, with version 0.6+, and also the Gurobi solver (you can get
a free academic version, just as the authors did) or the GLPK solvers, but do expect some
undefined behaviour if you plan to use GLPK, by the heavy use of lazy constraints.
With these two (three), it is enough to run the setup-julia.jl, which is a small script that
downloads all the required Julia packages via HTTPS. Latex with PGFPlots is not a necessity but
highly recommend for result visualization.

