#!/usr/bin/env julia
if !(pwd() in LOAD_PATH)
    push!(LOAD_PATH, pwd())
end

using ImplementationTypes
using BuildStage
using AssignStage
using Plots
using ArgParse
using YAML
using Utils
#using Cbc
#solver = CbcSolver()

solver = nothing
try
  using Gurobi
  solver = GurobiSolver()
catch 
  using GLPKMathProgInterface
  solver = GLPKSolverMIP()
end


gr()
# pyplot()
#plotlyjs()

s = ArgParseSettings()

@add_arg_table s begin
	"--non-grid"
		help = "Generate instance in a grid, instead of free."
		action = :store_true
	"--plot-all"
		help = "Plot all intermediate steps"
		action = :store_true
	"--exact"
		help = "Solve via MILP"
		action = :store_true
	"--localMethod"
		help = "Local Method optim. to use. (Default: ReSS)"
		arg_type = String
		default = "InO"
	"Specs"
		help = "Name of specs file for generation"
		arg_type = String
		default = "situation7-specs.yaml"
	"-l", "--label"
		help = "Label given to files of output"
		arg_type = String
		default = "results"
end

args = parse_args(s)

specsfiles = ["situation5-specs.yaml", "situation6-specs.yaml"]

for specfile in specsfiles
	name = first(split(specfile, "-"))
  results = Dict("exact" => zeros(0, 2), "OOS" => zeros(0, 2), "ReSC" => zeros(0, 2))

  println("Reading specs...")

  output_name = args["label"]
  specs = YAML.load(open(specfile))

  buildDict = @time buildStage(specs, verbose=true)

  println("Building model...")

  assignDict = @time buildModel(buildDict)

  println("Solving...")

	for t in 1:6
  #sol, val, iters, optim_iters = @time ARC2(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
  							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
              	#20, 200, CiROptim, 300, printStep=1)
  #if !args["exact"]
  	#if args["localMethod"] == "CiR"
      #sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
      							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                  	#20, 200, CiROptim, 300, printStep=5)
  	#elseif args["localMethod"] == "InO"
      #sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
      							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                  	#20, 200, InOOptim, 300, printStep=5)
  	#elseif args["localMethod"] == "OOS"
			gc()
      #ret, time, _, _ = @timed ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
      							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                  	#OOSOptim, maxARCIters=300, maxARCRepeats=10, maxRepeats=10, maxIters=100, printStep=20)
      #sol, val, iters, optim_iters = ret
    	#results["OOS"] = [results["OOS"]; [val["total"] time]]
    	#println("Time: $time")
      #println("Solved. ARC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)
  	#elseif args["localMethod"] == "ReSC"
			gc()
      #ret, time, _, _ = @timed ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
      							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                  	#ReSCOptim, maxARCIters=300, maxARCRepeats=10, maxRepeats=10, maxIters=100, printStep=20)
      #sol, val, iters, optim_iters = ret
    	#results["ReSC"] = [results["ReSC"]; [val["total"] time]]
    	#println("Time: $time")
      #println("Solved. ARC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)
  	#elseif args["localMethod"] == "Gen"
      #sol, val, iters, optim_iters = @time ARC(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
      							#assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
                  	#GenOptim, maxARCIters=1000, maxARCRepeats=10, maxRepeats=20, maxIters=2000, printStep=20)
  	#end
  #else
			gc()
      ret, time, _, _ = @timed MILP(assignDict["StartStates"], assignDict["EndStates"], assignDict["Orientations"],
      							assignDict["NodeCosts"], assignDict["Costs"], assignDict["Demand"], assignDict["Supply"], assignDict["Betas"],
      							solver)
      sol, val, iters, optim_iters = ret
    	results["exact"] = [results["exact"]; [val["total"] time]]
    	println("Time: $time")
      println("Solved. ARC Iters: ", iters, " OPTIM Iters: ", optim_iters, " Val: ", val)
  end

	for (k, v) in results
		writecsv(open(string(name, "-", k, ".csv"), "a"), v)
	end

end

