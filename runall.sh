tries=50
tries_exact=10
for mission in 5 6
do
  for lm in OOS ReSC
  do
  	out="tests-arc$lm-$mission.txt"
		touch $out
  	for t in `seq 1 $tries`
  	do
  		echo "$mission-$lm-$t"
  		julia build-and-assign.jl -l situation$mission situation$mission-specs.yaml --localMethod $lm > $out
		done
	done
done
			
for mission in 5 6
do
	out="tests-exact-$mission.txt"
	touch $out
	for t in `seq 1 $tries_exact`
	do
		echo "$mission-exact-$t"
		julia build-and-assign.jl -l situation$mission-exact situation$mission-specs.yaml --exact > $out
	done
done
			
		
		
