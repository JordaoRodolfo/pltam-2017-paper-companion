import pandas as pd

def main():
  results = pd.DataFrame({"Case": [], "Mean Time": [], "Best Value": [], "Mean Value": []})
  tests = ["OOS", "ReSC", "exact"]
  sits = ["situation5-", "situation6-"]
  for sit in sits:
    for test in tests:
      print("--------------------------------------"+sit+test)
      df = pd.read_csv(sit+test+".csv", header=None, names=["Value", "Time"])
      time = "${0} \pm {1}$".format(df["Time"].mean().round(2), df["Time"].std().round(2))
      val = "${0} \pm {1}$".format(df["Value"].mean().round(2), df["Value"].std().round(2))
      row = pd.DataFrame({"Case": [sit+test], "Mean Time": [time], "Best Value": [df["Value"].min()], "Mean Value": [val]})
      results = results.append(row)
      #with open(sit+test, "r") as f:
        #solline = ""
        #res = {"time": [], "solution": []}
        #found = False
        #for line in f:
          #if found and "Solved" in line:
            #times = solline.split()[0]
            #valdict = line.split()[-1]
            #commaind = valdict.index(",")
            #res["time"].append(float(times))
            #res["solution"].append(float(valdict[14:commaind]))
          #if "seconds" in line:
            #found = True
            #solline = line
          #else:
            #found = False
            #solline = line
        #df = pd.DataFrame(res)
      #print("mean")
      #print(df.mean())
      #print("std")
      #print(df.std())
      #print("min")
      #print(df.min())
  print(results.round(2).to_latex())

if __name__ == "__main__":
  main()
