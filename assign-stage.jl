if !(pwd() in LOAD_PATH)
    push!(LOAD_PATH, pwd())
end

using ImplementationTypes
using AssignStage
using Plots
using ArgParse
using YAML
using Utils

s = ArgParseSettings()

@add_arg_table s begin
		"inputData"
        help = "The instance declaration file. Only YAML supported now."
        arg_type = String
        default = "instance-assign.yaml"
end

args = parse_args(s)

println("Chosen parameters: ")
println("Instance: ", args["inputData"])

println("Starting printing engine...")

gr()
# pyplot()

println("Reading instance...")

instance = YAML.load(open(args["inputData"]))

println("Setting parameters...")

base_specs = instance["Base"]
N = instance["Nodes"]
agents = instance["Agents"]
num_agents = length(agents)
num_operations = instance["Operations"]

orientations = Dict{Int64, Float64}()
for (k, v) in instance["Orientations"]
    orientations[k] = v
end

num_orientations = length(orientations)
positions = instance["Positions"]
interest = instance["Interest"]
interest_nodes = instance["InterestNodes"]
boundaries = instance["Boundaries"]
obstacles = instance["Obstacles"]

println("Building model...")

betas = Dict(a => agents[a]["beta"] for a in keys(agents))

relation = Dict{Int64,Int64}()
invrelation = Dict{Int64,Int64}()
for (k, v) in enumerate(interest_nodes)
    relation[v] = k
    invrelation[k] = v
end

NodeCosts = zeros(N, num_operations)
for (n, c) in instance["NodeCosts"]
	NodeCosts[relation[n], :] = c
end
demand = NodeCosts .> 0.0

supply = zeros(num_agents, num_operations)
for (na, va) in agents
	for (no, vo) in va["operation"]
		supply[na, no] = 1
	end
end

pos = zeros(length(positions), 2)
for (k, v) in positions
	pos[k, :] = v
end
# println(instance["Positions"])
# Neighbors = Dict(1 => [2])
# for (node, neigh) in instance["Neighbors"]
# 	Neighbors[node] = neigh
# end
# TurnCosts = zeros(N, N, N, num_agents)
# for (i, v1) in instance["TurnCosts"]
# 	for (j, v2) in v1
# 		for (k, v3) in v2
# 			for (a, c) in enumerate(v3)
# 				TurnCosts[relation[i], relation[j], relation[k], a] = c
# 			end
# 		end
# 	end
# end

Costs = zeros(N, N, num_orientations, num_orientations, num_agents)
for (i, v1) in instance["Costs"]
    for (j, v2) in v1
        for (b, v3) in v2
            for (e, v4) in v3
                for (a, c) in enumerate(v4)
                    if isa(c, String)
                        Costs[relation[i], relation[j], b, e, a] = Inf
                    else
                        Costs[relation[i], relation[j], b, e, a] = c
                    end
                end
            end
        end
    end
end

Pathstrings = instance["Paths"]#Dict((1,2,1) => [1; 2])
Paths = Dict{Tuple{State, State, Int64}, Array{State, 1}}()
# sta = r"\((\d+),(\d+)\)"
# ag = r",(\d+)\)$"
for (k, v) in Pathstrings
    # println(k)
    ar = fill(State(1,1), length(v))
    tup = eval(parse(k))
    for (num, val) in v
        sta = eval(parse(val))
        ar[num] = sta
    end
    Paths[tup] = ar
    # agcap = match(ag, k)
    # println(tup)
end
# println(Paths)
# for (i, v1) in instance["Paths"]
# 	for (j, v2) in v1
# 		for (a, p) in v2
# 			Paths[(i,j,a)] = p
# 		end
# 	end
# end
startStates = Dict{Int64, State}()
for (a, s) in instance["BaseInbound"]
    sta = eval(parse(s))
    startStates[a] = State(relation[sta.node], sta.orientation)
end
endStates = Dict{Int64, State}()
for (a, s) in instance["BaseOutbound"]
    sta = eval(parse(s))
    endStates[a] = State(relation[sta.node], sta.orientation)
end

println("Solving...")

sol, val = ARC(startStates, endStates, orientations, NodeCosts, Costs, demand, supply, betas;
	tourMaxIters=500, maxIters=100, printStep=1)
# ARC(base_n, N, num_agents, Hval, MinPercent=0.05, SAtries=10)

println("Solved.")

println("Printing solution...")
instance_name, _ = split(basename(args["inputData"]), ".")
file_name, _ = split(basename(@__FILE__), ".")

outDict = Dict()
outDict["ObjFun"] = val
outDict["Base"] = instance["Base"]
outDict["Paths"] = sol
open("res-$file_name-$instance_name.yaml", "w") do f
	YAMLwrite(f, outDict)
end

#println(clusters)
println("Plotting solution...")

dpi = 300
plot_size = (800, 800)
base_pos = base_specs["pos"]
base_angle = base_specs["angle"]
# push!(boundaries, boundaries[1])
function dict2plot(d)
    B = zeros(length(d)+1, 2)
    for i in Iterators.take(eachindex(B), length(d))
        B[i, :] = d[i]
    end
    B[length(d)+1, :] = d[1]
    return B
end
B = dict2plot(boundaries)
plotinterest = plot(B[:, 1], B[:, 2], label="Boundary", dpi=dpi, size=plot_size, aspect_ratio=1.0)
for (n, area) in obstacles
    # push!(area["Boundaries"], first(area["Boundaries"]))
    B = dict2plot(area["Boundaries"])
    plot!(plotinterest, B[:, 1], B[:, 2], label="Obstacle $n")
end
for (n, area) in interest
    # push!(area["Boundaries"], first(area["Boundaries"]))
    B = dict2plot(area["Boundaries"])
    plot!(plotinterest, B[:, 1], B[:, 2], label="Interest $n")
    # plot!(plotinterest, [p[1] for p in area["Boundaries"]], [p[2] for p in area["Boundaries"]], label="Interest $n")
end
scatter!(plotinterest, [pos[i, 1] for i in interest_nodes], [pos[i, 2] for i in interest_nodes], label="nodes")
scatter!(plotinterest, [base_pos[1]], [base_pos[2]], label="Base")
plotpaths = deepcopy(plotinterest)

linestyles = Dict(1 => :dash, 2 => :dot)
colours = Dict(1 => :red, 2 => :blue)
for tour in sol
    a = tour.a
    # startState = startStates[a]
    # endState = endStates[a]
    for i in Iterators.take(eachindex(tour), length(tour)-1)
        s = State(invrelation[tour.states[i].node], tour.states[i].orientation)
        e = State(invrelation[tour.states[i+1].node], tour.states[i+1].orientation)
        b = first(Paths[(s, e, a)])
        for f in Iterators.drop(Paths[(s, e, a)], 1)
            p = buildBezier(b, f, agents[a]["minspeed"], pos, orientations)
            plot!(plotpaths, p.Px, p.Py, linspace(0.0, 1.0, 10), linestyle=linestyles[a], color=colours[a], label="")
            b = f
        end
    end
end
for a in 1:length(agents)
    bi = eval(parse(instance["BaseInbound"][a]))
    bo = eval(parse(instance["BaseOutbound"][a]))
    p = buildBezier(base_pos, base_angle, bi, agents[a]["minspeed"], pos, orientations)
    plot!(plotpaths, p.Px, p.Py, linspace(0.0, 1.0, 10), linestyle=linestyles[a], color=colours[a], label="")
    p = buildBezier(bo, base_pos, mod(base_angle+pi, 2*pi), agents[a]["minspeed"], pos, orientations)
    plot!(plotpaths, p.Px, p.Py, linspace(0.0, 1.0, 10), linestyle=linestyles[a], color=colours[a], label="")
end
# for i in interest_nodes
#     X(t) = pos[i, 1] + cover_radius*0.5*cos(t)
#     Y(t) = pos[i, 2] + cover_radius*0.5*sin(t)
#     plot!(plotinterest, X, Y, linspace(0.0, 2*pi, 15), linestyle=:dashdot, color=:black, label="")
# end

# bounds = zeros(length(boundaries), 2)
# for (nb, vb) in boundaries
# 	bounds[nb, :] = vb
# end
# bounds = [bounds; bounds[1, 1] bounds[1, 2]]
# plot(bounds[:,1], bounds[:,2], label="Boundary")
#
# for (ni, vi) in obstacles
# 	obsi = zeros(length(vi["Boundaries"])+1, 2)
# 	for (nb, vb) in vi["Boundaries"]
# 		obsi[nb, :] = vb
# 	end
# 	obsi[end, :] = obsi[1, :]
# 	plot!(obsi[:,1], obsi[:,2], label="Obstacles $ni")
# end
#
# inter = zeros(0, 2)
# for (ni, vi) in interest
# 	interi = zeros(length(vi["Boundaries"])+1, 2)
# 	for (nb, vb) in vi["Boundaries"]
# 		interi[nb, :] = vb
# 	end
# 	interi[end, :] = interi[1, :]
# 	plot!(interi[:,1], interi[:,2], label="Interest $ni")
# end
#
# nodes_interest = collect(keys(relation))
# scatter!(Positions[nodes_interest, 1], Positions[nodes_interest, 2], label="Nodes")
#
# base_n_x, base_n_y = Positions[base_n, :]
# scatter!([base_n_x], [base_n_y], label="Base")
#
# for a in 1:num_agents
# 	tours_a = filter(t -> t.a == a, sol)
# 	route = zeros(0, 2)
# 	for r in tours_a
# 		for i in 1:size(r.nodes, 1)-1
# 			for p in Paths[(invrelation[r.nodes[i]], invrelation[r.nodes[i+1]], a)]
# 				# println(p)
# 				route = [route; Positions[p, 1] Positions[p, 2]]
# 			end
# 		end
# 		route = [route; NaN NaN]
# 		# println(route)
# 	end
# 	plot!(route[:, 1], route[:, 2], label="Agent $a")
# end

savefig(plotpaths, "visual-res-$file_name-$instance_name.png")

# plot(J, label="Criteria")
#
# savefig("results/visual-res-objfun-$file_name-$instance_name.png")
#
# writecsv("results/res-objfun-$file_name-$instance_name.csv", J)

println("Done.")
